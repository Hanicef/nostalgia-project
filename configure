#!/bin/sh

if [ ! -e "configure" ]; then
	echo 'you must be in the same directory as the script to run it'
	exit 1
fi

lib_paths="/lib /usr/lib $(echo "$LD_LIBRARY_PATH" | tr ':' ' ')"

print_status() {
	printf "\e[1m%s..." "$1"
}

print_ok() {
	printf "\e[32mok \e[39m(%s)\e[0m\n" "$1"
}

print_fail() {
	printf "\e[31mfail\e[0m\n"
}

parse_ld() {
	while read -r line; do
		case $line in
			'include '*)
				for file in $(echo "$line" | cut -d ' ' -f 2); do
					parse_ld "$file"
				done
				;;
			'#'*)
				;;
			*)
				lib_paths="$lib_paths $line"
				;;
		esac
	done < "$1"
}

if [ -e /etc/ld.so.conf ]; then
	parse_ld /etc/ld.so.conf
fi

find_cmd() {
	print_status "locating $1"
	if cmd="$(command -v "$1")"; then
		print_ok "$cmd"
		return 0
	fi
	print_fail
	return 1
}

if find_cmd gcc || find_cmd clang; then
	cc="$cmd"
else
	echo 'cannot find suitable compiler'
	exit 1
fi

find_lib() {
	print_status "locating $1"
	for path in $lib_paths; do
		if [ -e "$path/$1" ]; then
			lib="$path/$1"
			print_ok "$lib"
			return 0
		fi
	done
	print_fail
	return 1
}

if ! find_lib libgnutls.so; then
	echo 'cannot find libgnutls.so'
	exit 1
fi

CFLAGS="$CFLAGS -Wall -Wextra -Werror=implicit-function-declaration -Werror=implicit-int"
CFLAGS="-g -O0 -D_POSIX_C_SOURCE=200809L -std=c17 -pthread"
build_hash="$(git log -n1 --format=format:%H)"
CFLAGS="$CFLAGS -DBUILD_HASH='"'"'$build_hash'"'"'"
LDLIBS="-lgnutls"

objs="$(find src/ -name '*.c' | tr '\n' ' ' | sed 's/\.c/\.o/g')"

printf 'OBJS := %s\n' "$objs" > Makefile
printf '\n' >> Makefile
printf 'CC ?= %s\n' "$cc" >> Makefile
printf 'CFLAGS += %s\n' "$CFLAGS" >> Makefile
printf 'LDLIBS += %s\n' "$LDLIBS" >> Makefile
printf '\n' >> Makefile
printf '.PHONY: all\n' >> Makefile
printf 'all: nostalgia-project\n' >> Makefile
printf '\n' >> Makefile
printf 'nostalgia-project: $(OBJS)\n' >> Makefile
printf '	$(CC) $(CFLAGS) -o $@ $(OBJS) $(LDLIBS)\n' >> Makefile
printf '\n' >> Makefile
printf '$(OBJS): %%.o: %%.c\n' >> Makefile
printf '	$(CC) $(CFLAGS) -c -o $@ $<\n' >> Makefile
printf '\n' >> Makefile
printf '.PHONY: clean\n' >> Makefile
printf 'clean:\n' >> Makefile
printf '	rm -f $(OBJS) nostalgia-project\n' >> Makefile
