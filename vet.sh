#/bin/sh
# Copyright (C) 2022  Gustaf "Hanicef" Alhäll
#
# This file is part of Nostalgia Project.
#
# Nostalgia Project is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Nostalgia Project is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License along
# with Nostalgia Project. If not, see <https://www.gnu.org/licenses/>.

for file in $(find . -name '*.c' -or -name '*.h'); do
	n=1
	while IFS='' read -r line; do
		if [ "$(printf "%s\n" "$line" | grep '	')" != '' ]; then
			printf "%s:%s: line contains tabs\n" "$file" "$n"
		fi
		if [ "$(printf "%s\n" "$line" | grep -E '^ *//')" = '' ]; then
			columns="$(printf "%s" "$line" | wc -c)"
			if [ $columns -gt 80 ]; then
				printf "%s:%s: line exceeds 80 characters (%s)\n" "$file" "$n" "$columns"
			fi
		fi
		n=$(expr $n + 1)
	done < $file
done
