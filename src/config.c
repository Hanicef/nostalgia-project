/*
    Copyright (C) 2022  Gustaf "Hanicef" Alhäll

    This file is part of Nostalgia Project.

    Nostalgia Project is free software: you can redistribute it and/or modify it
    under the terms of the GNU Affero General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    Nostalgia Project is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Nostalgia Project. If not, see <https://www.gnu.org/licenses/>.
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <inttypes.h>

#include "config.h"
#include "utils.h"

struct config config = {
    .server = {
        .address = "",
        .port = 1965,
        .host = "localhost",
    },
    .tls = {
        .certificate = "/etc/ssl/private/nostalgia-project.pem",
    },
    .archiver = {
        .archive_path = "/srv/nostalgia-project",
        .file_size_limit = 1048576, // 1 MiB
        .blacklist_path = "/etc/nostalgia-project/blacklist.txt",
        .archive_interval = 60,
    },
    .misc = {
        .doc_path = "/srv/nostalgia-project/docs",
    },
};

static ssize_t read_line(int fd, char *buf, size_t size) {
    size_t len = 0;
    while (len < size) {
        ssize_t count = read(fd, &buf[len++], 1);
        if (count == -1) {
            return -1;
        } else if (count == 0) {
            buf[--len] = '\0';
            break;
        } else if (buf[len-1] == '\n') {
            buf[len] = '\0';
            break;
        }
    }

    return len;
}

static bool parse_line(size_t n, char *line, char **key, char **value) {
    size_t i = 0;
    while (isspace(line[i])) i++;
    if (line[i] == '\0' || line[i] == '#' || line[i] == ';') {
        *key = NULL;
        *value = NULL;
        return true;
    }

    bool section = (line[i] == '[');
    if (section) {
        i++;
    }
    while (isspace(line[i])) i++;
    *key = &line[i];
    while (isalnum(line[i]) || line[i] == '_') {
        i++;
    }
    if (*key == &line[i]) {
        log_format("configuration error: line %zd: expected key", n);
        return false;
    }

    // save the index so we don't overwrite the delimeter in case there are no
    // spaces before it.
    size_t end = i;
    while (isspace(line[i])) i++;

    if (section) {
        if (line[i] != ']') {
            log_format("configuration error: line %zd: "
                "expected closing bracket", n);
            return false;
        }
        line[end] = '\0';
        *value = NULL;
        return true;
    }

    if (line[i] != '=') {
        log_format("configuration error: line %zd: expected '='", n);
        return false;
    }

    line[end] = '\0';
    for (i++; isspace(line[i]); i++);
    *value = &line[i];
    if (line[i] == '\0') {
        return true;
    }

    for (i++; line[i] != '\0'; i++);
    for (i--; *value < &line[i] && isspace(line[i]); i--);
    line[i+1] = '\0';
    return true;
}

enum assign_status {
    ASSIGN_OK,
    ASSIGN_FAIL,
    ASSIGN_MALFORMATTED,
    ASSIGN_FATAL,
};

typedef enum assign_status (*assign_t)(size_t n, char *, char *);

static enum assign_status assign_server(size_t n, char *key, char *value) {
    if (strcmp(key, "address") == 0) {
        config.server.address = value;
        return ASSIGN_OK;
    } else if (strcmp(key, "port") == 0) {
        char *end;
        config.server.port = strtol(value, &end, 10);
        if (end[0] != '\0') {
            log_format("configuration error: line %zd: expected number for "
                "'port' in section 'server'", n);
            return ASSIGN_MALFORMATTED;
        }
        free(value);
        return ASSIGN_OK;
    } else if (strcmp(key, "host") == 0) {
        config.server.host = value;
        return ASSIGN_OK;
    } else {
        return ASSIGN_FAIL;
    }
}

static enum assign_status assign_tls(size_t n, char *key, char *value) {
    if (strcmp(key, "certificate") == 0) {
        config.tls.certificate = value;
        return ASSIGN_OK;
    } else {
        return ASSIGN_FAIL;
    }
}

static uint64_t get_scale_suffix(char *s) {
    size_t len = strlen(s);
    if (len > 1 && s[len-1] == 'B') {
        switch (s[len-2]) {
            case 'K': s[len-2] = '\0'; return 1000ll;
            case 'M': s[len-2] = '\0'; return 1000000ll;
            case 'G': s[len-2] = '\0'; return 1000000000ll;
            case 'T': s[len-2] = '\0'; return 1000000000000ll;
            case 'P': s[len-2] = '\0'; return 1000000000000000ll;
            case 'E': s[len-2] = '\0'; return 1000000000000000000ll;
            case 'i': break;
            default: return 1;
        }
    }

    char *c = &s[len-1];
    if (*c == 'B') {
        assert(s[len-2] == 'i');
        if (len <= 3) {
            return 1;
        }
        c = &s[len-3];
    }

    switch (*c) {
        case 'K': *c = '\0'; return (1ll << 10);
        case 'M': *c = '\0'; return (1ll << 20);
        case 'G': *c = '\0'; return (1ll << 30);
        case 'T': *c = '\0'; return (1ll << 40);
        case 'P': *c = '\0'; return (1ll << 50);
        case 'E': *c = '\0'; return (1ll << 60);
        default: return 1;
    }
}

static uint64_t get_time_suffix(char *s) {
    size_t len = strlen(s);
    char *c = &s[len-1];
    switch (*c) {
        case 's': *c = '\0'; return 1;
        case 'm': *c = '\0'; return 60;
        case 'h': *c = '\0'; return 60 * 60;
        case 'D': *c = '\0'; return 60 * 60 * 24;
        case 'W': *c = '\0'; return 60 * 60 * 24 * 7;
        case 'M': *c = '\0'; return 60 * 60 * 24 * 30;
        case 'Y': *c = '\0'; return 60 * 60 * 24 * 365;
        default: return 1;
    }
}

static enum assign_status assign_archiver(size_t n, char *key, char *value) {
    if (strcmp(key, "archive_path") == 0) {
        config.archiver.archive_path = value;
        return ASSIGN_OK;
    } else if (strcmp(key, "file_size_limit") == 0) {
        char *end;
        uint64_t scale = get_scale_suffix(value);
        config.archiver.file_size_limit = strtoul(value, &end, 10) * scale;
        if (end[0] != '\0') {
            log_format("configuration error: line %zd: expected number for "
                "'file_size_limit' in section 'archiver'", n);
            return ASSIGN_MALFORMATTED;
        }
        free(value);
        return ASSIGN_OK;
    } else if (strcmp(key, "blacklist_path") == 0) {
        config.archiver.blacklist_path = value;
        return ASSIGN_OK;
    } else if (strcmp(key, "archive_interval") == 0) {
        char *end;
        uint64_t scale = get_time_suffix(value);
        config.archiver.archive_interval = strtoul(value, &end, 10) * scale;
        if (end[0] != '\0') {
            log_format("configuration error: line %zd: expected number for "
                "'archive_interval' in section 'archiver'", n);
            return ASSIGN_MALFORMATTED;
        }
        free(value);
        return ASSIGN_OK;
    } else {
        return ASSIGN_FAIL;
    }
}

static enum assign_status assign_misc(size_t n, char *key, char *value) {
    if (strcmp(key, "doc_path") == 0) {
        config.misc.doc_path = value;
        return ASSIGN_OK;
    } else {
        return ASSIGN_FAIL;
    }
}

enum section {
    SERVER,
    TLS,
    ARCHIVER,
    MISC,
    SECTION_COUNT,
};

assign_t sections[SECTION_COUNT] = {
    assign_server,
    assign_tls,
    assign_archiver,
    assign_misc,
};

char const *section_names[SECTION_COUNT] = {
    "server",
    "tls",
    "archiver",
    "misc",
};

#define BUF_SIZE 1024

bool load_config(char const *file) {
    char *buf = malloc(BUF_SIZE);
    if (buf == NULL) {
        log_line("not enough memory to allocate line buffer");
        return NULL;
    }
    int fd = open(file, O_RDONLY);
    if (fd == -1) {
        log_format("failed to open %s: %s", file, errno_string(errno));
        free(buf);
        return NULL;
    }

    enum section section = SECTION_COUNT;
    bool ok = true;
    for (size_t linenum = 1;; linenum++) {
        ssize_t len = read_line(fd, buf, BUF_SIZE);
        if (len == -1) {
            log_format("failed to read from %s: %s", file, errno_string(errno));
            ok = false;
            break;
        } else if (len == 0) {
            break;
        } else if (len == BUF_SIZE) {
            log_format("failed to read from %s: line too long", file);
            ok = false;
            break;
        }
        if (buf[len-1] == '\n') {
            buf[--len] = '\0';
        }

        char *key, *value;
        if (!parse_line(linenum, buf, &key, &value)) {
            ok = false;
            continue;
        }

        if (key == NULL) {
            continue;
        }
        if (value == NULL) {
            for (section = 0; section < SECTION_COUNT; section++) {
                if (strcmp(key, section_names[section]) == 0) {
                    break;
                }
            }
            if (section == SECTION_COUNT) {
                log_format("configuration error: line %zd: unknown section "
                    "'%s'", linenum, key);
                ok = false;
                continue;
            }
        } else {
            if (section == SECTION_COUNT) {
                log_format("configuration error: line %zd: missing section",
                    linenum);
                ok = false;
                continue;
            }
            char *field = malloc(strlen(value) + 1);
            if (field == NULL) {
                log_line("failed to allocate memory for configuration");
                ok = false;
                break;
            }
            strcpy(field, value);

            enum assign_status status = sections[section](linenum, key, field);
            if (status != ASSIGN_OK) {
                if (status == ASSIGN_FAIL) {
                    log_format("configuration error: line %zd: unknown key "
                        "'%s' in section '%s'", linenum, key,
                        section_names[section]);
                }
                free(field);
                ok = false;
                if (status == ASSIGN_FATAL) {
                    break;
                }
            }
        }
    }

    free(buf);
    close(fd);
    return ok;
}
