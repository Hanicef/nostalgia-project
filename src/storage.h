/*
    Copyright (C) 2022  Gustaf "Hanicef" Alhäll

    This file is part of Nostalgia Project.

    Nostalgia Project is free software: you can redistribute it and/or modify it
    under the terms of the GNU Affero General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    Nostalgia Project is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Nostalgia Project. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STORAGE_H
#define STORAGE_H

#include <time.h>
#include <stdbool.h>

#include "hash.h"

enum search_status {
    SEARCH_MATCH,
    SEARCH_NO_MATCH,
    SEARCH_CONTINUE,
    SEARCH_ERROR,
};

typedef enum search_status (*search_fn)(time_t, char const[static HASH_WIDTH],
    char const *, void *);

bool add_index(char const *domain, char const *path, time_t date,
    char const *ctype, char const content_hash[static HASH_WIDTH]);
enum search_status search_index(char const *domain, char const *path,
    search_fn callback, void *data);

bool add_redirect(int fd, char const *from, char const *to);
bool has_redirect(int fd, char const *from, char const *to);
char **get_redirects(int fd, char const *from, size_t *count);

#endif // STORAGE_H
