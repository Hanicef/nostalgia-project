/*
    Copyright (C) 2022  Gustaf "Hanicef" Alhäll

    This file is part of Nostalgia Project.

    Nostalgia Project is free software: you can redistribute it and/or modify it
    under the terms of the GNU Affero General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    Nostalgia Project is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Nostalgia Project. If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>

#include "utils.h"

char const *log_file;

void log_line(char const *s) {
    time_t timeval = time(NULL);
    struct tm timefmt;
    char timestr[20];

    size_t len = strftime(timestr, sizeof(timestr), "%FT%T",
        localtime_r(&timeval, &timefmt));
    assert(len == sizeof(timestr)-1);
    if (log_file == NULL) {
        if (fprintf(stderr, "[%s] %s\n", timestr, s) < 0) {
            abort();
        }
    } else {
        int fd = open(log_file, O_WRONLY | O_CREAT | O_APPEND, 0600);
        if (fd == -1) {
            // if we can't create the log file, abort. we don't want to lose
            // log data.
            abort();
        }
        if (dprintf(fd, "[%s] %s\n", timestr, s) < 0) {
            abort();
        }
        close(fd);
    }
}

void log_format(char const *format, ...) {
    va_list args;
    va_start(args, format);
    size_t len = vsnprintf(NULL, 0, format, args);
    va_end(args);

    char *line = malloc(sizeof(char) * ++len);
    if (line == NULL) {
        abort();
    }

    va_start(args, format);
    len = vsnprintf(line, len, format, args);
    va_end(args);
    log_line(line);
    free(line);
}

bool init_string(struct string_buffer *buf, size_t cap) {
    assert(buf != NULL);
    assert(cap > 0);
    buf->cap = cap;
    buf->size = 0;
    buf->s = malloc(cap * sizeof(char));
    return (buf->s != NULL);
}

void append_string(struct string_buffer *buf, char const *s) {
    size_t len = strlen(s);
    assert(buf->size + len < buf->cap);
    strcpy(&buf->s[buf->size], s);
    buf->size += len;
}

char *close_string(struct string_buffer *buf) {
    buf->s[buf->size] = '\0';
    return buf->s;
}

bool has_prefix(char const *s, char const *prefix) {
    size_t i;
    for (i = 0; s[i] != '\0' && prefix[i] != '\0'; i++) {
        if (s[i] != prefix[i]) {
            return false;
        }
    }
    return prefix[i] == '\0';
}

bool has_prefix_case(char const *s, char const *prefix) {
    size_t i;
    for (i = 0; s[i] != '\0' && prefix[i] != '\0'; i++) {
        if (tolower(s[i]) != tolower(prefix[i])) {
            return false;
        }
    }
    return prefix[i] == '\0';
}

char *join_path(char const *s, ...) {
    assert(s != NULL);
    va_list args;
    size_t len = strlen(s);
    if (s[len-1] == '/') {
        len -= 1;
    }

    va_start(args, s);
    for (;;) {
        char const *a = va_arg(args, char const *);
        if (a == NULL) {
            break;
        }
        size_t alen = strlen(a);
        len += alen;
        if (a[0] != '/') {
            len += 1;
        }
        if (a[alen-1] == '/') {
            len -= 1;
        }
    }
    va_end(args);

    struct string_buffer buf;
    if (!init_string(&buf, len + 1)) {
        return NULL;
    }

    append_string(&buf, s);
    if (buf.s[buf.size-1] == '/') {
        buf.size--;
    }
    va_start(args, s);
    for (;;) {
        char const *a = va_arg(args, char const *);
        if (a == NULL) {
            break;
        }
        if (a[0] != '/') {
            append_string(&buf, "/");
        }
        append_string(&buf, a);
        if (buf.s[buf.size-1] == '/') {
            buf.size--;
        }
    }
    va_end(args);
    return close_string(&buf);
}

char **split_path(char const *s, size_t *count, size_t max) {
    if (s[0] == '/') {
        s = &s[1];
    }
    char *sc = malloc(sizeof(char) * (strlen(s) + 1));
    if (sc == NULL) {
        return NULL;
    }
    strcpy(sc, s);

    char **out;
    *count = 1;
    size_t i;
    for (i = 0; sc[i] != '\0'; i++) {
        if (sc[i] == '/') {
            (*count)++;
        }
    }
    if (sc[i-1] == '/') {
        (*count)--;
    }
    if (*count > max) {
        *count = max;
    }

    out = calloc(sizeof(char *), *count);
    if (out == NULL) {
        free(sc);
        return NULL;
    }
    size_t n;
    for (i = 0, n = 0; n < *count; n++) {
        char *c = strchr(&sc[i], '/');
        if (c == NULL) {
            out[n++] = &sc[i];
            break;
        }
        c[0] = '\0';
        out[n] = &sc[i];
        i = c - sc + 1;
    }
    assert(n == *count);
    return out;
}

char *format_hex_string(char s[static 17], uint64_t v) {
    for (int i = 15; i >= 0; i--) {
        char c = v & 0xf;
        if (c < 10) {
            s[i] = c + '0';
        } else {
            s[i] = c + 'a' - 10;
        }
        v >>= 4;
    }
    s[16] = '\0';
    return s;
}

static char base64_char(unsigned char c) {
    if (c < 26) {
        return 'A' + c;
    } else if (c >= 26 && c < 52) {
        return 'a' + c - 26;
    } else if (c >= 52 && c < 62) {
        return '0' + c - 52;
    } else if (c == 62) {
        return '-';
    } else if (c == 63) {
        return '_';
    } else {
        abort();
    }
}

size_t format_base64(char *s, size_t len, char const *data, size_t dlen) {
    char c;
    size_t i, j = 0;
    for (i = 0; i < dlen; i++) {
        switch (i % 3) {
            case 0:
                s[j++] = base64_char((unsigned char)data[i] >> 2);
                c = (data[i] & 0x03) << 4;
                break;

            case 1:
                s[j++] = base64_char(((unsigned char)data[i] >> 4) | c);
                c = (data[i] & 0x0f) << 2;
                break;

            case 2:
                s[j++] = base64_char(((unsigned char)data[i] >> 6) | c);
                if (j >= len) {
                    return j;
                }
                s[j++] = base64_char(data[i] & 0x3f);
                break;
        }
        if (j >= len) {
            return j;
        }
    }

    if (i % 3 != 0) {
        s[j++] = base64_char(c);
        if (j >= len) {
            return j;
        }

        s[j++] = '=';
        if (j >= len) {
            return j;
        }

        if (i % 3 == 1) {
            s[j++] = '=';
            if (j >= len) {
                return j;
            }
        }
    }

    s[j] = '\0';
    return j;
}

char const *errno_string(int err) {
    // NOTE: we roll our own error strings as strerror is not thread safe.
    switch (err) {
        case E2BIG: return "Argument list too long";
        case EACCES: return "Permission denied";
        case EADDRINUSE: return "Address in use";
        case EADDRNOTAVAIL: return "Address not available";
        case EAFNOSUPPORT: return "Address family not supported";
        case EAGAIN: return "Resource unavailable, try again";
        case EALREADY: return "Connection already in progress";
        case EBADF: return "Bad file descriptor";
        case EBADMSG: return "Bad message";
        case EBUSY: return "Device or resource busy";
        case ECANCELED: return "Operation canceled";
        case ECHILD: return "No child processes";
        case ECONNABORTED: return "Connection aborted";
        case ECONNREFUSED: return "Connection refused";
        case ECONNRESET: return "Connection reset";
        case EDEADLK: return "Resource deadlock would occur";
        case EDESTADDRREQ: return "Destination address required";
        case EDOM: return "Mathematics argument out of domain of function";
        case EDQUOT: return "Reserved";
        case EEXIST: return "File exists";
        case EFAULT: return "Bad address";
        case EFBIG: return "File too large";
        case EHOSTUNREACH: return "Host is unreachable";
        case EIDRM: return "Identifier removed";
        case EILSEQ: return "Illegal byte sequence";
        case EINPROGRESS: return "Operation in progress";
        case EINTR: return "Interrupted function";
        case EINVAL: return "Invalid argument";
        case EIO: return "I/O error";
        case EISCONN: return "Socket is connected";
        case EISDIR: return "Is a directory";
        case ELOOP: return "Too many levels of symbolic links";
        case EMFILE: return "File descriptor value too large";
        case EMLINK: return "Too many links";
        case EMSGSIZE: return "Message too large";
        case EMULTIHOP: return "Reserved";
        case ENAMETOOLONG: return "Filename too long";
        case ENETDOWN: return "Network is down";
        case ENETRESET: return "Connection aborted by network";
        case ENETUNREACH: return "Network unreachable";
        case ENFILE: return "Too many files open in system";
        case ENOBUFS: return "No buffer space available";
        case ENODATA: return "No message is available on the STREAM head read "
            "queue";
        case ENODEV: return "No such device";
        case ENOENT: return "No such file or directory";
        case ENOEXEC: return "Executable file format error";
        case ENOLCK: return "No locks available";
        case ENOLINK: return "Reserved";
        case ENOMEM: return "Not enough space";
        case ENOMSG: return "No message of the desired type";
        case ENOPROTOOPT: return "Protocol not available";
        case ENOSPC: return "No space left on device";
        case ENOSR: return "No STREAM resources";
        case ENOSTR: return "Not a STREAM";
        case ENOSYS: return "Functionality not supported";
        case ENOTCONN: return "The socket is not connected";
        case ENOTDIR: return "Not a directory or a symbolic link to a "
            "directory";
        case ENOTEMPTY: return "Directory not empty";
        case ENOTRECOVERABLE: return "State not recoverable";
        case ENOTSOCK: return "Not a socket";
        case ENOTSUP: return "Not supported";
        case ENOTTY: return "Inappropriate I/O control operation";
        case ENXIO: return "No such device or address";
#if EOPNOTSUPP != ENOTSUP
        case EOPNOTSUPP: return "Operation not supported on socket";
#endif
        case EOVERFLOW: return "Value too large to be stored in data type";
        case EOWNERDEAD: return "Previous owner died";
        case EPERM: return "Operation not permitted";
        case EPIPE: return "Broken pipe";
        case EPROTO: return "Protocol error";
        case EPROTONOSUPPORT: return "Protocol not supported";
        case EPROTOTYPE: return "Protocol wrong type for socket";
        case ERANGE: return "Result too large";
        case EROFS: return "Read-only file system";
        case ESPIPE: return "Invalid seek";
        case ESRCH: return "No such process";
        case ESTALE: return "Reserved";
        case ETIME: return "Stream ioctl() timeout";
        case ETIMEDOUT: return "Connection timed out";
        case ETXTBSY: return "Text file busy";
#if EWOULDBLOCK != EAGAIN
        case EWOULDBLOCK: return "Operation would block";
#endif
        case EXDEV: return "Cross-device link";
        default: abort();
    }
}
