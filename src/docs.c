/*
    Copyright (C) 2022  Gustaf "Hanicef" Alhäll

    This file is part of Nostalgia Project.

    Nostalgia Project is free software: you can redistribute it and/or modify it
    under the terms of the GNU Affero General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    Nostalgia Project is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Nostalgia Project. If not, see <https://www.gnu.org/licenses/>.
*/

#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>

#include "docs.h"
#include "utils.h"

size_t doccount;
struct docfile *docfiles;

static void unload_docs(void) {
    for (size_t i = 0; i < doccount; i++) {
        free(docfiles[i].title);
        free(docfiles[i].name);
    }
    free(docfiles);
    docfiles = NULL;
}

static char *read_title(int fd) {
    char c = ' ';
    while (isspace(c)) {
        if (read(fd, &c, sizeof(c)) != sizeof(c)) {
            return NULL;
        }
    }
    if (c != '#') {
        return NULL;
    }
    if (read(fd, &c, sizeof(c)) != sizeof(c) || c == '#') {
        return NULL;
    }
    while (isblank(c)) {
        if (read(fd, &c, sizeof(c)) != sizeof(c)) {
            return NULL;
        }
    }
    char *buf = malloc(256);
    if (buf == NULL) {
        return NULL;
    }

    size_t i;
    for (i = 0; i < 256 && c != '\n'; i++) {
        buf[i] = c;
        switch (read(fd, &c, sizeof(c))) {
            case -1:
                free(buf);
                return NULL;

            case 0:
                c = '\n';
                break;

            default:
                break;
        }
    }
    if (i == 0 || i >= 256) {
        free(buf);
        return NULL;
    }

    buf = realloc(buf, sizeof(char) * (i + 1));
    if (buf == NULL) {
        return NULL;
    }
    buf[i] = '\0';
    return buf;
}

bool load_docs(char const *path) {
    DIR *dir = opendir(path);
    if (dir == NULL) {
        log_format("failed to open documentation directory: %s",
            errno_string(errno));
        return false;
    }

    for (;;) {
        errno = 0;
        struct dirent *entry = readdir(dir);
        if (entry == NULL) {
            if (errno != 0) {
                log_format("failed to list documentation: %s",
                    errno_string(errno));
                closedir(dir);
                unload_docs();
                return false;
            }
            break;
        }
        if (entry->d_name[0] == '.') {
            continue;
        }

        char *abspath = join_path(path, entry->d_name, NULL);
        if (abspath == NULL) {
            log_line("not enough memory to allocate documentation");
            closedir(dir);
            unload_docs();
            return false;
        }

        int fd = open(abspath, O_RDONLY);
        if (fd == -1) {
            log_format("failed to open %s: %s", abspath, errno_string(errno));
            free(abspath);
            closedir(dir);
            unload_docs();
            return false;
        }
        free(abspath);

        char *name = malloc(sizeof(char) * (strlen(entry->d_name) + 1));
        if (name == NULL) {
            log_line("not enough memory to allocate documentation");
            close(fd);
            closedir(dir);
            unload_docs();
            return false;
        }
        strcpy(name, entry->d_name);
        docfiles = realloc(docfiles, sizeof(struct docfile) * ++doccount);
        if (docfiles == NULL) {
            log_line("not enough memory to allocate documentation");
            free(name);
            close(fd);
            closedir(dir);
            doccount--;
            unload_docs();
            return false;
        }
        docfiles[doccount-1].name = name;
        docfiles[doccount-1].title = read_title(fd);
        errno = 0;
        if (docfiles[doccount-1].title == NULL) {
            if (errno != 0) {
                log_format("failed to parse title: %s", errno_string(errno));
                free(name);
                close(fd);
                closedir(dir);
                doccount--;
                unload_docs();
                return false;
            }
            log_format("unable to locate title on %s; using file name", name);
        }
        close(fd);
    }

    closedir(dir);
    return true;
}
