/*
    Copyright (C) 2022  Gustaf "Hanicef" Alhäll

    This file is part of Nostalgia Project.

    Nostalgia Project is free software: you can redistribute it and/or modify it
    under the terms of the GNU Affero General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    Nostalgia Project is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Nostalgia Project. If not, see <https://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "hash.h"

#define ROTL(x, n) (((x) << (n)) | ((x) >> (sizeof(x) * 8 - (n))))
#define ROTR(x, n) (((x) >> (n)) | ((x) << (sizeof(x) * 8 - (n))))

#define STATE_B 1600
#define STATE_W 64
#define STATE_L 6

#define STATE_WIDTH 25

#define ROUNDS 24 // 12 + 2 * STATE_L

#define KECCAK_C 512
#define KECCAK_R 1088
#define BUF_WIDTH 136

static_assert(KECCAK_C + KECCAK_R == STATE_B);
static_assert(KECCAK_R / 8 == BUF_WIDTH);

struct hash {
    size_t buflen;
    union {
        uint64_t state[STATE_WIDTH];
        char state8[STATE_WIDTH * 8];
    };
    char buf[BUF_WIDTH];
};

static inline uint64_t *keccak_theta(uint64_t state[static STATE_WIDTH]) {
    uint64_t c[5];
    for (size_t x = 0; x < 5; x++) {
        c[x] = state[x] ^ state[x + 5] ^ state[x + 10] ^ state[x + 15] ^
            state[x + 20];
    }
    for (size_t x = 0; x < 5; x++) {
        uint64_t d = c[(x + 4) % 5] ^ ROTL(c[(x + 1) % 5], 1);
        for (size_t y = 0; y < 5; y++) {
            state[x + y * 5] ^= d;
        }
    }
    return state;
}

static inline uint64_t *keccak_rho(uint64_t state[static STATE_WIDTH]) {
    uint64_t tmp[STATE_WIDTH];
    tmp[0] = state[0];
    size_t x = 1, y = 0;
    for (size_t t = 0; t < 24; t++) {
        ssize_t shift = (t + 1) * (t + 2) / 2;
        tmp[x + y * 5] = ROTL(state[x + y * 5], shift % STATE_W);
        size_t n = x;
        x = y;
        y = (2 * n + 3 * y) % 5;
    }
    memcpy(state, tmp, sizeof(tmp));
    return state;
}

static inline uint64_t *keccak_pi(uint64_t state[static STATE_WIDTH]) {
    uint64_t tmp[STATE_WIDTH];
    for (size_t x = 0; x < 5; x++) {
        for (size_t y = 0; y < 5; y++) {
            size_t xd = y;
            size_t yd = (2 * x + 3 * y) % 5;
            tmp[xd + yd * 5] = state[x + y * 5];
        }
    }
    memcpy(state, tmp, sizeof(tmp));
    return state;
}

static inline uint64_t *keccak_chi(uint64_t state[static STATE_WIDTH]) {
    uint64_t tmp[STATE_WIDTH];
    for (size_t x = 0; x < 5; x++) {
        for (size_t y = 0; y < 5; y++) {
            tmp[x + y * 5] = state[x + y * 5] ^
                (~state[(x + 1) % 5 + y * 5] & state[(x + 2) % 5 + y * 5]);
        }
    }
    memcpy(state, tmp, sizeof(tmp));
    return state;
}

static inline char keccak_rc(size_t t) {
    uint16_t r = 1;
    for (size_t i = 0; i < t % 255; i++) {
        r <<= 1;
        if (r & 0x100) {
            r ^= 0x71;
        }
    }
    return r & 1;
}

static inline uint64_t *keccak_iota(uint64_t state[static STATE_WIDTH],
        size_t round) {
    for (size_t j = 0; j <= STATE_L; j++) {
        state[0] ^= (uint64_t)keccak_rc(j + 7 * round) << ((1 << j) - 1);
    }
    return state;
}

static void keccak(uint64_t state[static STATE_WIDTH]) {
    for (size_t i = 0; i < ROUNDS; i++) {
        keccak_iota(keccak_chi(keccak_pi(keccak_rho(keccak_theta(state)))), i);
    }
}

struct hash *hash_init(void) {
    struct hash *hash = malloc(sizeof(struct hash));
    if (hash == NULL) {
        return NULL;
    }

    memset(hash->state, 0, sizeof(hash->state));
    hash->buflen = 0;
    return hash;
}

void hash_insert(struct hash *hash, char const *buf, size_t size) {
    for (size_t i = 0; i < size; i++) {
        hash->buf[hash->buflen++] = buf[i];
        if (hash->buflen >= BUF_WIDTH) {
            for (size_t j = 0; j < BUF_WIDTH; j++) {
                hash->state8[j] ^= hash->buf[j];
            }
            keccak(hash->state);
            hash->buflen = 0;
        }
    }
}

char *hash_digest(struct hash *hash, char data[static HASH_WIDTH]) {
    for (size_t j = 0; j < hash->buflen; j++) {
        hash->state8[j] ^= hash->buf[j];
    }
    hash->state8[hash->buflen] ^= 0x06;
    hash->state8[BUF_WIDTH-1] ^= 0x80;
    keccak(hash->state);
    return memcpy(data, hash->state, HASH_WIDTH);
}
