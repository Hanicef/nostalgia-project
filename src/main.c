/*
    Copyright (C) 2022  Gustaf "Hanicef" Alhäll

    This file is part of Nostalgia Project.

    Nostalgia Project is free software: you can redistribute it and/or modify it
    under the terms of the GNU Affero General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    Nostalgia Project is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Nostalgia Project. If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <string.h>
#include <stdbool.h>
#include <signal.h>
#include <pwd.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include "client.h"
#include "config.h"
#include "docs.h"
#include "tls.h"
#include "utils.h"

static void daemonize(char const *pid_file, char const *user) {
    sigset_t set;
    sigemptyset(&set);
    if (sigprocmask(SIG_SETMASK, &set, NULL) == -1) {
        log_format("failed to clear signal mask: %s", errno_string(errno));
        exit(EXIT_FAILURE);
    }

    int pipefd[2];
    if (pipe(pipefd) == -1) {
        log_format("failed to create fork pipe: %s", errno_string(errno));
        exit(EXIT_FAILURE);
    }

    bool ok;
    switch (fork()) {
        case -1:
            log_format("failed to fork to background: %s", errno_string(errno));
            exit(EXIT_FAILURE);

        default:
            close(pipefd[1]);
            ssize_t count = read(pipefd[0], &ok, sizeof(ok));
            if (count == -1) {
                log_format("failed to fork to background: %s",
                    errno_string(errno));
                close(pipefd[0]);
                exit(EXIT_FAILURE);
            }
            exit(ok ? EXIT_SUCCESS : EXIT_FAILURE);

        case 0:
            close(pipefd[0]);
            if (setsid() == -1) {
                log_format("failed to create session: %s", errno_string(errno));
                ok = false;
                break;
            }

            switch (fork()) {
                case -1:
                    log_format("failed to fork daemon: %s",
                        errno_string(errno));
                    ok = false;
                    break;

                default:
                    exit(EXIT_SUCCESS);

                case 0:
                    umask(0);
                    if (chdir("/") == -1) {
                        log_format("failed to move to root directory: %s",
                            errno_string(errno));
                        ok = false;
                        break;
                    }

                    int fd = open(pid_file, O_WRONLY | O_CREAT | O_TRUNC, 0600);
                    if (fd == -1) {
                        log_format("failed to move to root directory: %s",
                            errno_string(errno));
                        ok = false;
                        break;
                    }

                    pid_t pid = getpid();
                    char pid_str[21];
                    size_t count = snprintf(pid_str, sizeof(pid_str), "%d",
                        pid);
                    assert(count < sizeof(pid_str));
                    if (write(fd, pid_str, strlen(pid_str)) == -1) {
                        log_format("failed to write pid file: %s",
                            errno_string(errno));
                        ok = false;
                        break;
                    }

                    if (user != NULL && getuid() == 0) {
                        errno = 0;
                        struct passwd *pw = getpwnam(user);
                        if (pw == NULL) {
                            if (errno != 0) {
                                log_format("failed to get user information: %s",
                                    errno_string(errno));
                            } else {
                                log_format("unable to drop privileges, user %s "
                                    "doesn't exist", user);
                            }
                            ok = false;
                            break;
                        }

                        if (setuid(pw->pw_uid) == -1) {
                            log_format("failed to switch user: %s",
                                errno_string(errno));
                            ok = false;
                            break;
                        }
                        if (setgid(pw->pw_gid) == -1) {
                            log_format("failed to switch group: %s",
                                errno_string(errno));
                            ok = false;
                            break;
                        }
                    } else if (user != NULL) {
                        log_line("cannot drop privileges as they are already "
                            "dropped");
                    }

                    // we do this last so we can report errors to the user
                    fd = open("/dev/null", O_RDWR);
                    if (fd == -1) {
                        log_format("failed to open /dev/null: %s",
                            errno_string(errno));
                        ok = false;
                        break;
                    }

                    if (dup2(fd, STDIN_FILENO) == -1 ||
                            dup2(fd, STDOUT_FILENO) == -1 ||
                            dup2(fd, STDERR_FILENO) == -1) {
                        log_format("failed to redirect stdio to /dev/null: %s",
                            errno_string(errno));
                        ok = false;
                        break;
                    }

                    ok = true;
                    break;
            }
            break;
    }
    write(pipefd[1], &ok, sizeof(ok));
    close(pipefd[1]);
    if (!ok) {
        exit(EXIT_FAILURE);
    }
}

static int server_socket;

static size_t client_count = 0;

static char const *get_signal_str(int sig) {
    switch (sig) {
        case SIGABRT: return "SIGABRT";
        case SIGALRM: return "SIGALRM";
        case SIGBUS: return "SIGBUS";
        case SIGCHLD: return "SIGCHLD";
        case SIGCONT: return "SIGCONT";
        case SIGFPE: return "SIGFPE";
        case SIGHUP: return "SIGHUP";
        case SIGILL: return "SIGILL";
        case SIGINT: return "SIGINT";
        case SIGKILL: return "SIGKILL";
        case SIGPIPE: return "SIGPIPE";
        case SIGPOLL: return "SIGPOLL";
        case SIGPROF: return "SIGPROF";
        case SIGQUIT: return "SIGQUIT";
        case SIGSEGV: return "SIGSEGV";
        case SIGSTOP: return "SIGSTOP";
        case SIGTSTP: return "SIGTSTP";
        case SIGSYS: return "SIGSYS";
        case SIGTERM: return "SIGTERM";
        case SIGTRAP: return "SIGTRAP";
        case SIGTTIN: return "SIGTTIN";
        case SIGTTOU: return "SIGTTOU";
        case SIGURG: return "SIGURG";
        case SIGUSR1: return "SIGUSR1";
        case SIGUSR2: return "SIGUSR2";
        case SIGVTALRM: return "SIGVTALRM";
        case SIGXCPU: return "SIGXCPU";
        case SIGXFSZ: return "SIGXFSZ";
        default: return "UNKNOWN";
    }
}

static void release_child(int sig) {
    siginfo_t info;
    for (;;) {
        if (waitid(P_ALL, 0, &info, WEXITED | WNOHANG) == -1) {
            if (errno == EINTR) {
                continue;
            } else if (errno == ECHILD) {
                break;
            }
            log_format("failed to check child status: %s", strerror(errno));
            break;
        }
        if (info.si_pid == 0) {
            break;
        }
        switch (info.si_code) {
            case CLD_EXITED:
                break;

            case CLD_KILLED:
                log_format("process %d was killed! (caused by %s)",
                    info.si_pid, get_signal_str(info.si_status));
                break;

            case CLD_DUMPED:
                log_format("process %d crashed! (caused by %s)", info.si_pid,
                    get_signal_str(info.si_status));
                break;

            default:
                log_format("process %d exited for unknown reasons",
                    info.si_pid);
                break;
        }
    }
    assert(client_count > 0);
    client_count--;
}

static void exit_gracefully(int sig) {
    log_format("caught signal %d, exiting...", sig);
    close(server_socket);
    while (client_count > 0) {
        pause();
    }
}

static void dispatch_client(int sock) {
    switch (fork()) {
        case -1:
            log_format("failed to dispatch client: %s", strerror(errno));
            close(sock);
            return;

        case 0:
            signal(SIGTERM, SIG_DFL);
            signal(SIGCHLD, SIG_DFL);
            handle_client(sock);
            exit(EXIT_SUCCESS);

        default:
            client_count++;
            close(sock);
            return;
    }
}

int main(int argc, char **argv) {
    // clear $TZ to default to UTC time
    setenv("TZ", "", 1);
    tzset();

    signal(SIGPIPE, SIG_IGN);

    char const *config_file = "/etc/nostalgia-project/config.ini";
    bool daemon = false;
    char const *pid_file = "/run/nostalgia-project.pid";
    char const *user = NULL;
    for (;;) {
        int opt = getopt(argc, argv, "Vl:c:Bp:U:");
        if (opt == -1) {
            break;
        }
        switch (opt) {
            case 'V':
                printf("Nostalgia Project - Git build %s\n", BUILD_HASH);
                return EXIT_SUCCESS;

            case 'l':
                log_file = optarg;
                break;

            case 'c':
                config_file = optarg;
                break;

            case 'B':
                daemon = true;
                break;

            case 'p':
                pid_file = optarg;
                break;

            case 'U':
                user = optarg;
                break;

            default:
                return EXIT_FAILURE;
        }
    }

    if (!load_config(config_file)) {
        return EXIT_FAILURE;
    }

    if (!init_tls(config.tls.certificate, config.server.host)) {
        return EXIT_FAILURE;
    }

    if (!load_docs(config.misc.doc_path)) {
        log_line("documentation will not be available to the client");
    }

    if (daemon) {
        daemonize(pid_file, user);
    }

    server_socket = socket(AF_INET6, SOCK_STREAM, 0);
    if (server_socket == -1) {
        log_format("failed to open socket: %s", errno_string(errno));
        return EXIT_FAILURE;
    }

    int b = 1;
    if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &b,
            sizeof(b)) == -1) {
        log_format("failed to mark socket as reusable: %s",
            errno_string(errno));
        close(server_socket);
        return EXIT_FAILURE;
    }

    struct sockaddr_in6 addr = {
        .sin6_family = AF_INET6,
        .sin6_port = htons(config.server.port),
        .sin6_flowinfo = 0,
        .sin6_addr = IN6ADDR_ANY_INIT,
        .sin6_scope_id = 0,
    };
    if (config.server.address[0] != '\0') {
        if (inet_pton(AF_INET6, config.server.address, &addr.sin6_addr) != 1) {
            log_line("failed to parse hostname: address not valid");
            close(server_socket);
            return EXIT_FAILURE;
        }
    }
    if (bind(server_socket, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
        log_format("failed to bind address: %s", errno_string(errno));
        close(server_socket);
        return EXIT_FAILURE;
    }

    if (listen(server_socket, 64) == -1) {
        log_format("failed to listen for connections: %s", errno_string(errno));
        close(server_socket);
        return EXIT_FAILURE;
    }

    struct sigaction action;
    action.sa_handler = exit_gracefully;
    sigemptyset(&action.sa_mask);
    action.sa_flags = SA_RESETHAND;
    if (sigaction(SIGTERM, &action, NULL) == -1) {
        log_format("failed to bind signal handler: %s", errno_string(errno));
        close(server_socket);
        return EXIT_FAILURE;
    }

    action.sa_handler = release_child;
    sigemptyset(&action.sa_mask);
    action.sa_flags = SA_NOCLDSTOP;
    if (sigaction(SIGCHLD, &action, NULL) == -1) {
        log_format("failed to bind signal handler: %s", errno_string(errno));
        close(server_socket);
        return EXIT_FAILURE;
    }

    for (;;) {
        int client = accept(server_socket, NULL, NULL);
        if (client == -1) {
            if (errno == EBADF) {
                // server socket is closed, so we shut down here
                return EXIT_SUCCESS;
            } else if (errno != EINTR) {
                log_format("failed to accept client: %s", errno_string(errno));
            }
            continue;
        }

        dispatch_client(client);
    }
}
