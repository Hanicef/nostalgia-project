/*
    Copyright (C) 2022  Gustaf "Hanicef" Alhäll

    This file is part of Nostalgia Project.

    Nostalgia Project is free software: you can redistribute it and/or modify it
    under the terms of the GNU Affero General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    Nostalgia Project is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Nostalgia Project. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TLS_H
#define TLS_H

#include <stdbool.h>

bool init_tls(char const *cert, char const *host);
void *open_tls(int sock, unsigned int timeout);
void *connect_tls(int sock, char const *domain, unsigned int timeout);
void close_tls(void *conn);

void set_tls_timeout(void *conn, unsigned int ms);
ssize_t read_tls(void *conn, void *buf, size_t count);
ssize_t write_tls(void *conn, void const *buf, size_t count);

#endif // TLS_H
