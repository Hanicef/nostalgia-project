/*
    Copyright (C) 2022  Gustaf "Hanicef" Alhäll

    This file is part of Nostalgia Project.

    Nostalgia Project is free software: you can redistribute it and/or modify it
    under the terms of the GNU Affero General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    Nostalgia Project is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Nostalgia Project. If not, see <https://www.gnu.org/licenses/>.
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdint.h>
#include <time.h>
#include <ctype.h>
#include <inttypes.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/file.h>

#include "client.h"
#include "config.h"
#include "crawl.h"
#include "docs.h"
#include "tls.h"
#include "utils.h"
#include "storage.h"

// NOTE: the strict upper limit is 1024, but we are giving some leeway for
// archived websites that uses long paths (prefixed with /view/date/domain).
#define LINE_MAX (1024 + 64)
#define WRITE_BUF_SIZE 256

#define RATE 2.0
#define PER 1.0

#define PATH_ESCAPE_CHARS "!$&'()*+,;=?#[]% "
#define QUERY_ESCAPE_CHARS "!$'()*+,;#[]% "

static char const gemini_response[] = "text/gemini; charset=utf-8";

ssize_t read_line(void *conn, char *buf, size_t bufsize) {
    size_t len = 0;
    while (len < bufsize) {
        char c;
        ssize_t count = read_tls(conn, &c, sizeof(c));
        if (count == -1) {
            return -1;
        } else if (count == 0) {
            buf[len] = '\0';
            return len;
        }
        if (c == '\n') {
            if (len > 0 && buf[len-1] == '\r') {
                buf[--len] = '\0';
            }
            return len;
        }

        buf[len++] = c;
    }
    buf[bufsize-1] = '\0';
    return bufsize;
}

ssize_t read_file_line(int fd, char *buf, size_t bufsize) {
    for (size_t len = 0; len < bufsize; len++) {
        ssize_t count = read(fd, &buf[len], sizeof(char));
        if (count == -1) {
            return -1;
        } else if (count == 0) {
            buf[len] = '\0';
            return len;
        }

        if (buf[len] == '\n') {
            buf[len] = '\0';
            return len;
        }
    }
    buf[bufsize-1] = '\0';
    return bufsize;
}

static char *unescape(char *s) {
    size_t i, j;
    for (i = 0, j = 0; s[j] != '\0'; i++, j++) {
        if (s[j] == '%') {
            char c = 0;
            for (size_t k = 1; k <= 2; k++) {
                c <<= 4;
                if (s[j+k] >= '0' && s[j+k] <= '9') {
                    c |= s[j+k] - '0';
                } else if (s[j+k] >= 'a' && s[j+k] <= 'f') {
                    c |= s[j+k] - 'a' + 10;
                } else if (s[j+k] >= 'A' && s[j+k] <= 'F') {
                    c |= s[j+k] - 'A' + 10;
                } else {
                    return NULL;
                }
            }

            s[i] = c;
            j += 2;
        } else {
            s[i] = s[j];
        }
    }
    s[i] = '\0';
    return s;
}

static char *parse_domain(char const *s, int *port) {
    assert(s != NULL);
    assert(port != NULL);
    size_t len;
    if (s[0] == '[') {
        for (len = 0; s[len] != '\0' && s[len] != ']'; len++);
        if (s[len] == '\0') {
            return NULL;
        }
    } else {
        for (len = 0; isalnum(s[len]) || s[len] == '.' || s[len] == '-'; len++);
    }

    char *domain = malloc(sizeof(char) * (len + 1));
    if (domain == NULL) {
        return NULL;
    }
    memcpy(domain, s, len);
    domain[len] = '\0';
    if (unescape(domain) == NULL) {
        free(domain);
        return NULL;
    }

    if (s[0] == '[') {
        len++;
    }
    if (s[len] == ':') {
        char *end;
        *port = strtoul(&s[len+1], &end, 10);
        if (end[0] != '\0' && end[0] != '/' && end[0] != '?' && end[0] != '#') {
            free(domain);
            return NULL;
        }
    } else {
        *port = 1965;
    }
    return domain;
}

bool parse_uri(struct uri *uri, char const *s) {
    size_t i;
    memset(uri, 0, sizeof(*uri));
    for (i = 0; isalnum(s[i]) || s[i] == '+' || s[i] == '-' || s[i] == '.';
        i++);
    if (s[i] == ':') {
        if (!has_prefix(s, "gemini:")) {
            return false;
        }
        s = &s[i+1];
        if (!has_prefix(s, "//")) {
            return false;
        }
        s = &s[2];
    }
    uri->domain = parse_domain(s, &uri->port);

    char const *path = strchr(s, '/'), *query = strchr(s, '?');
    if (path != NULL && (query == NULL || path < query)) {
        size_t len = (query == NULL) ? strlen(path) : (size_t)(query - path);
        uri->path = malloc(sizeof(char) * (len + 1));
        if (uri->path == NULL) {
            free(uri->domain);
            return false;
        }

        memcpy(uri->path, path, len);
        uri->path[len] = '\0';
        if (unescape(uri->path) == NULL) {
            free(uri->path);
            free(uri->domain);
            return false;
        }
    } else {
        uri->path = malloc(sizeof(char));
        if (uri->path == NULL) {
            free(uri->domain);
            return false;
        }
        uri->path[0] = '\0';
    }

    if (query != NULL) {
        uri->query = malloc(sizeof(char) * strlen(query));
        if (uri->query == NULL) {
            free(uri->path);
            free(uri->domain);
            return false;
        }
        strcpy(uri->query, &query[1]);
        if (unescape(uri->query) == NULL) {
            free(uri->query);
            free(uri->path);
            free(uri->domain);
            return false;
        }
    }
    return true;
}

static char *escape(char const *s, char const *reserved) {
    size_t len = 0;
    for (size_t i = 0; s[i] != '\0'; i++) {
        if (strchr(reserved, s[i]) != NULL) {
            len += 3;
        } else {
            len++;
        }
    }

    char *out = malloc(len + 1);
    if (out == NULL) {
        return NULL;
    }

    size_t j = 0;
    for (size_t i = 0; s[i] != '\0'; i++) {
        if (strchr(reserved, s[i]) != NULL) {
            out[j++] = '%';
            char c = s[i] >> 4;
            if (c < 10) {
                out[j++] = c + '0';
            } else {
                out[j++] = c + 'a' - 10;
            }
            c = s[i] & 0xf;
            if (c < 10) {
                out[j++] = c + '0';
            } else {
                out[j++] = c + 'a' - 10;
            }
        } else {
            out[j++] = s[i];
        }
        assert(j <= len);
    }
    out[j] = '\0';
    return out;
}

char *format_uri(struct uri *uri) {
    char *path = escape(uri->path, PATH_ESCAPE_CHARS);
    if (path == NULL) {
        return NULL;
    }

    char *uri_string;
    if (uri->query != NULL) {
        char *query = escape(uri->query, QUERY_ESCAPE_CHARS);
        if (query == NULL) {
            free(path);
            return NULL;
        }
        size_t uri_len = sizeof("gemini://?") + strlen(uri->domain) +
            strlen(path) + strlen(query);
        uri_string = malloc(uri_len);
        if (uri_string == NULL) {
            free(query);
            free(path);
            return NULL;
        }

        size_t len = snprintf(uri_string, uri_len, "gemini://%s%s?%s",
            uri->domain, path, query);
        assert(len < uri_len);
        free(query);
    } else {
        size_t uri_len = sizeof("gemini://") + strlen(uri->domain) +
            strlen(path);
        uri_string = malloc(uri_len);
        if (uri_string == NULL) {
            free(path);
            return NULL;
        }

        size_t len = snprintf(uri_string, uri_len, "gemini://%s%s", uri->domain,
            path);
        assert(len < uri_len);
    }
    free(path);
    return uri_string;
}

void free_uri(struct uri *uri) {
    free(uri->query);
    free(uri->path);
    free(uri->domain);
}

static char const front_page[] =
"# Nostalgia Project - The Gemini Archiver\r\n"
"\r\n"
"=>/search View a website in the past\r\n"
"=>/archive Request a website to be archived\r\n"
"\r\n"
"## Documentation\r\n"
"\r\n";

static char const footer[] =
"\r\nNostalgia Project - Git build "BUILD_HASH"\r\n";

static char const archive_success[] =
"# Success\r\n"
"\r\n"
"The capsule %s has been archived successfully.\r\n"
"=>/search?%s You can view the archived version here.\r\n";

char const out_of_memory_msg[] = "Server ran out of memory";
static char const out_of_memory_resp[] = "41 Server ran out of memory\r\n";

bool write_header(void *conn, int status, char const *message) {
    assert(status >= 10 && status <= 69);

    size_t len = sizeof("00 \r\n") + strlen(message);
    char *line = malloc(len);
    if (line == NULL) {
        log_line("not enough memory to allocate response path");
        write_tls(conn, out_of_memory_resp, sizeof(out_of_memory_resp) - 1);
        return false;
    }

    size_t count = snprintf(line, len, "%d %s\r\n", status, message);
    assert(count < len);
    bool ok = (write_tls(conn, line, count) != -1);
    free(line);
    return ok;
}

static bool is_domain_sane(char const *domain) {
    return (domain[0] != '.' && strchr(domain, '/') == NULL);
}

static char **list_redirects(struct uri *uri, size_t *count) {
    char *path = join_path(config.archiver.archive_path, uri->domain,
        "redirects", NULL);
    if (path == NULL) {
        log_line("not enough memory to allocate redirect path");
        return NULL;
    }
    int fd = open(path, O_RDONLY);
    if (fd == -1) {
        if (errno != ENOENT) {
            log_format("failed to access %s: %s", path, errno_string(errno));
        } else {
            errno = 0;
        }
        free(path);
        return NULL;
    }
    free(path);

    errno = 0;
    char **paths = get_redirects(fd, uri->path, count);
    if (paths == NULL && errno != 0) {
        log_format("failed to read redirect file: %s", errno_string(errno));
        close(fd);
        return NULL;
    }
    close(fd);
    return paths;
}

struct times {
    size_t count;
    time_t *times;
};

static enum search_status add_time(time_t date, char const hash[HASH_WIDTH],
        char const *ctype, void *data) {
    struct times *times = data;
    time_t *tmp_times = realloc(times->times, sizeof(time_t) * ++times->count);
    if (tmp_times == NULL) {
        free(times->times);
        return SEARCH_ERROR;
    }
    tmp_times[times->count - 1] = date;
    times->times = tmp_times;
    // return SEARCH_NO_MATCH so we can iterate through all entries.
    return SEARCH_NO_MATCH;
}

static time_t *list_entries(struct uri *uri, size_t *count) {
    struct times times_data = {
        .count = 0,
        .times = NULL,
    };
    enum search_status status;
    if (uri->query != NULL) {
        size_t len = strlen(uri->path) + strlen(uri->query) + 2;
        struct string_buffer path_buf;
        if (!init_string(&path_buf, len)) {
            log_line("not enough memory to allocate query uri");
            return false;
        }
        append_string(&path_buf, uri->path);
        append_string(&path_buf, "?");
        append_string(&path_buf, uri->query);
        char *index_path = close_string(&path_buf);
        status = search_index(uri->domain, index_path, add_time, &times_data);
        free(index_path);
    } else {
        status = search_index(uri->domain, uri->path, add_time, &times_data);
    }
    if (status == SEARCH_ERROR) {
        log_format("failed to read index file %s: %s", uri->domain,
            errno_string(errno));
        return NULL;
    }
    errno = 0;
    *count = times_data.count;
    return times_data.times;
}

static bool list_archive(void *conn, struct uri *uri) {
    if (!is_domain_sane(uri->domain)) {
        write_header(conn, STATUS_PERMANENT_FAILURE,
            "Requested domain is invalid");
        return false;
    }

    size_t entry_count;
    time_t *entries = list_entries(uri, &entry_count);
    if (entries == NULL && errno != 0) {
        write_header(conn, STATUS_TEMPORARY_FAILURE,
            "Something went wrong when listing entries");
        return NULL;
    }

    size_t redirect_count;
    char **redirects = list_redirects(uri, &redirect_count);
    if (redirects == NULL) {
        if (errno != 0) {
            write_header(conn, STATUS_TEMPORARY_FAILURE,
                "Something went wrong when listing entries");
            free(entries);
            return NULL;
        }
        redirect_count = 0;
    }

    if (entries == NULL && redirects == NULL) {
        write_header(conn, STATUS_NOT_FOUND, "Entry hasn't been archived");
        return NULL;
    }

    char *path = escape(uri->path, PATH_ESCAPE_CHARS);
    if (path == NULL) {
        while (redirect_count-- > 0) {
            free(redirects[redirect_count]);
        }
        free(redirects);
        free(entries);
        return false;
    }

    char *query = NULL;
    if (uri->query) {
        query = escape(uri->query, QUERY_ESCAPE_CHARS);
        if (query == NULL) {
            free(path);
            while (redirect_count-- > 0) {
                free(redirects[redirect_count]);
            }
            free(redirects);
            free(entries);
            return false;
        }
    }

    size_t domainlen = strlen(uri->domain);
    size_t pathlen = strlen(uri->path);
    size_t querylen = 0;
    if (query != NULL) {
        querylen = strlen(query) + 1;
    }
    size_t contentlen = sizeof(
        "=>/view/18446744073709551615/ 0000-00-00T00:00:00\r\n") + domainlen +
        pathlen + querylen;
    char *content = malloc(contentlen);
    if (content == NULL) {
        log_line("not enough memory to allocate domain response");
        write_header(conn, STATUS_SERVER_UNAVAILABLE, out_of_memory_msg);
        free(path);
        free(query);
        while (redirect_count-- > 0) {
            free(redirects[redirect_count]);
        }
        free(redirects);
        free(entries);
        return false;
    }

    if (!write_header(conn, STATUS_SUCCESS, gemini_response)) {
        free(path);
        free(query);
        while (redirect_count-- > 0) {
            free(redirects[redirect_count]);
        }
        free(redirects);
        free(entries);
        free(content);
        return false;
    }
    size_t count = snprintf(content, contentlen, "# Snapshots of %s%s\r\n",
        uri->domain, uri->path);
    assert(count < contentlen);
    if (write_tls(conn, content, count) == -1) {
        free(path);
        free(query);
        while (redirect_count-- > 0) {
            free(redirects[redirect_count]);
        }
        free(redirects);
        free(entries);
        free(content);
        return false;
    }

    if (entries != NULL) {
        static char const entry_header[] = "\r\n## Entries\r\n\r\n";
        if (write_tls(conn, entry_header, sizeof(entry_header)-1) == -1) {
            free(path);
            free(query);
            while (redirect_count-- > 0) {
                free(redirects[redirect_count]);
            }
            free(redirects);
            free(entries);
            free(content);
            return false;
        }

        for (size_t i = 0; i < entry_count; i++) {
            struct tm timedef;
            char date[20];
            size_t count = strftime(date, sizeof(date), "%FT%T",
                gmtime_r(&entries[i], &timedef));
            assert(count < sizeof(date));

            if (query != NULL) {
                count = snprintf(content, contentlen,
                    "=>/view/%"PRIu64"/%s%s?%s %s\r\n", (uint64_t)entries[i],
                    uri->domain, path, query, date);
            } else {
                count = snprintf(content, contentlen,
                    "=>/view/%"PRIu64"/%s%s %s\r\n", (uint64_t)entries[i],
                    uri->domain, path, date);
            }
            assert(count < contentlen);
            if (write_tls(conn, content, count) == -1) {
                free(path);
                free(query);
                while (redirect_count-- > 0) {
                    free(redirects[redirect_count]);
                }
                free(redirects);
                free(entries);
                free(content);
                return false;
            }
        }
    }
    free(entries);
    free(path);
    free(query);

    if (redirects != NULL) {
        static char const redirect_header[] = "\r\n## Redirects\r\n\r\n";
        if (write_tls(conn, redirect_header, sizeof(redirect_header)-1) == -1) {
            while (redirect_count-- > 0) {
                free(redirects[redirect_count]);
            }
            free(redirects);
            free(content);
            return false;
        }

        for (size_t i = 0; i < redirect_count; i++) {
            char const *dest = redirects[i];
            if (dest[0] == '\0') {
                dest = "(root)";
            }

            count = snprintf(content, contentlen,
                "=>/search?gemini://%s%s %s\r\n", uri->domain, redirects[i],
                dest);
            assert(count < contentlen);
            if (write_tls(conn, content, count) == -1) {
                while (redirect_count-- > 0) {
                    free(redirects[redirect_count]);
                }
                free(redirects);
                free(content);
                return false;
            }
        }
    }

    while (redirect_count-- > 0) {
        free(redirects[redirect_count]);
    }
    free(redirects);
    free(content);
    return true;
}

struct index_data {
    time_t date;
    char *ctype;
    char *hash;
};

static enum search_status entry_matches(time_t date,
        char const hash[static HASH_WIDTH], char const *ctype, void *data) {
    struct index_data *index_data = data;
    if (index_data->date == date) {
        index_data->ctype = malloc(strlen(ctype) + 1);
        if (index_data->ctype == NULL) {
            return SEARCH_ERROR;
        }
        strcpy(index_data->ctype, ctype);
        strcpy(index_data->hash, hash);
        return SEARCH_MATCH;
    }
    return SEARCH_NO_MATCH;
}

static bool write_content(void *conn, int fd) {
    char *buf = malloc(WRITE_BUF_SIZE * sizeof(char));
    if (buf == NULL) {
        log_line("not enough memory to allocate line buffer");
        return false;
    }
    for (;;) {
        ssize_t count = read(fd, buf, WRITE_BUF_SIZE * sizeof(char));
        switch (count) {
            case -1:
                log_format("failed to read content: %s", errno_string(errno));
                free(buf);
                return false;

            case 0:
                free(buf);
                return true;

            default:
                if (write_tls(conn, buf, count) == -1) {
                    free(buf);
                    return false;
                }
                break;
        }
    }
}

static bool write_gemini(void *conn, int fd, char const *domain) {
    int link_part = 1;
    char *buf = malloc(WRITE_BUF_SIZE * sizeof(char));
    if (buf == NULL) {
        log_line("not enough memory to allocate line buffer");
        return false;
    }

    size_t urn_len = sizeof("gemini://") + strlen(domain) - 1;
    char *urn = malloc(urn_len + 1);
    if (urn == NULL) {
        log_line("not enough memory to allocate urn");
        free(buf);
        return false;
    }
    strcpy(urn, "gemini://");
    strcat(urn, domain);

    for (;;) {
        ssize_t count = read(fd, buf, WRITE_BUF_SIZE * sizeof(char));
        switch (count) {
            case -1:
                log_format("failed to read content: %s", errno_string(errno));
                free(urn);
                free(buf);
                return false;

            case 0:
                free(urn);
                free(buf);
                return true;

            default:
                break;
        }

        size_t offset = 0;
        for (size_t i = 0; i < (size_t)count; i++) {
            if (link_part == 3) {
                if (buf[i] == ' ') {
                    continue;
                }
                if (buf[i] == '/') {
                    // correct links so paths point to the original domain.
                    if (write_tls(conn, &buf[offset], i - offset) == -1) {
                        free(urn);
                        free(buf);
                        return false;
                    }
                    if (write_tls(conn, urn, urn_len) == -1) {
                        free(urn);
                        free(buf);
                        return false;
                    }
                    offset = i;
                }
                link_part = 0;
            } else if (buf[i] == '\n') {
                link_part = 1;
            } else if (link_part == 1 && buf[i] == '=') {
                link_part = 2;
            } else if (link_part == 2 && buf[i] == '>') {
                link_part = 3;
            } else {
                link_part = 0;
            }
        }

        if (offset < (size_t)count) {
            if (write_tls(conn, &buf[offset], count - offset) == -1) {
                free(urn);
                free(buf);
                return false;
            }
        }
    }
}

static bool view_archive(void *conn, struct uri *uri) {
    size_t count;
    char **dirs = split_path(&uri->path[sizeof("/view/") - 1], &count, 3);
    if (dirs == NULL) {
        log_line("not enough memory to allocate path split");
        write_header(conn, STATUS_SERVER_UNAVAILABLE, out_of_memory_msg);
        return false;
    }

    if (count < 1) {
        write_header(conn, STATUS_PERMANENT_FAILURE, "Date is missing");
        free(dirs[0]);
        free(dirs);
        return false;
    }

    if (count < 2) {
        write_header(conn, STATUS_PERMANENT_FAILURE, "Domain is missing");
        free(dirs[0]);
        free(dirs);
        return false;
    }

    char *end;
    time_t date = strtoull(dirs[0], &end, 10);
    if (end[0] != '\0') {
        write_header(conn, STATUS_PERMANENT_FAILURE,
            "Requested date is invalid");
        free(dirs[0]);
        free(dirs);
        return false;
    }

    if (!is_domain_sane(dirs[1])) {
        write_header(conn, STATUS_PERMANENT_FAILURE,
            "Requested domain is invalid");
        free(dirs[0]);
        free(dirs);
        return false;
    }
    char const *path = &uri->path[sizeof("/view/") + strlen(dirs[0]) +
        strlen(dirs[1])];

    char content_hash[HASH_WIDTH];
    struct index_data data = {
        .date = date,
        .ctype = NULL,
        .hash = content_hash,
    };
    enum search_status status;
    if (uri->query == NULL) {
        status = search_index(dirs[1], path, entry_matches, &data);
    } else {
        struct string_buffer path_domain;
        if (!init_string(&path_domain, strlen(path) + strlen(uri->query) + 2)) {
            log_line("not enough memory to allocate archive path");
            free(dirs[0]);
            free(dirs);
            write_header(conn, STATUS_SERVER_UNAVAILABLE, out_of_memory_msg);
            return false;
        }
        append_string(&path_domain, path);
        append_string(&path_domain, "?");
        append_string(&path_domain, uri->query);

        status = search_index(dirs[1], close_string(&path_domain),
            entry_matches, &data);
        free(path_domain.s);
    }
    if (status != SEARCH_MATCH) {
        if (status == SEARCH_NO_MATCH) {
            write_header(conn, STATUS_NOT_FOUND, "Entry doesn't exist");
        } else {
            log_format("failed to read index file: %s",
                errno_string(errno));
            write_header(conn, STATUS_TEMPORARY_FAILURE,
                "Something went wrong when fetching content");
        }
        free(dirs[0]);
        free(dirs);
        return false;
    }
    char *ctype = data.ctype;

    char hash_str[45];
    size_t len = format_base64(hash_str, sizeof(hash_str), content_hash,
        sizeof(content_hash));
    assert(len < sizeof(hash_str));
    char *filepath = join_path(config.archiver.archive_path, "content",
        hash_str, NULL);
    if (filepath == NULL) {
        log_line("not enough memory to allocate archive path");
        free(ctype);
        free(dirs[0]);
        free(dirs);
        write_header(conn, STATUS_SERVER_UNAVAILABLE, out_of_memory_msg);
        return false;
    }
    int content_fd = open(filepath, O_RDONLY);
    if (content_fd == -1) {
        log_format("failed to open content file: %s", errno_string(errno));
        free(filepath);
        free(ctype);
        free(dirs[0]);
        free(dirs);
        write_header(conn, STATUS_TEMPORARY_FAILURE,
            "Something went wrong when fetching content");
        return false;
    }
    free(filepath);

    if (!write_header(conn, STATUS_SUCCESS, ctype)) {
        close(content_fd);
        free(ctype);
        free(dirs[0]);
        free(dirs);
        return false;
    }
    if (has_prefix(ctype, "text/gemini")) {
        write_gemini(conn, content_fd, dirs[1]);
    } else {
        write_content(conn, content_fd);
    }

    close(content_fd);
    free(ctype);
    free(dirs[0]);
    free(dirs);
    return true;
}

static void view_document(void *conn, char const *name) {
    for (size_t i = 0; i < doccount; i++) {
        if (strcmp(docfiles[i].name, name) == 0) {
            char *path = join_path(config.misc.doc_path, name, NULL);
            if (path == NULL) {
                log_line("not enough memory to allocate document path");
                write_header(conn, STATUS_SERVER_UNAVAILABLE,
                    out_of_memory_msg);
                return;
            }

            int fd = open(path, O_RDONLY);
            if (fd == -1) {
                log_format("failed to open %s: %s", path, errno_string(errno));
                free(path);
                write_header(conn, STATUS_TEMPORARY_FAILURE,
                    "Something went wrong when fetching content");
                return;
            }
            free(path);

            if (!write_header(conn, STATUS_SUCCESS, gemini_response)) {
                close(fd);
                return;
            }
            write_content(conn, fd);
            close(fd);
            return;
        }
    }
    write_header(conn, STATUS_NOT_FOUND, "Resounce doesn't exist");
}

static void handle_path(void *conn, struct uri *req) {
    if (req->path[0] == '\0' || (req->path[0] == '/' && req->path[1] == '\0')) {
        if (!write_header(conn, STATUS_SUCCESS, gemini_response)) {
            return;
        }

        if (write_tls(conn, front_page, sizeof(front_page)-1) == -1) {
            return;
        }

        for (size_t i = 0; i < doccount; i++) {
            size_t entrysize = sizeof("=>/docs/ \r\n") +
                strlen(docfiles[i].name) + strlen(docfiles[i].title);
            char *entry = malloc(entrysize);
            if (entry == NULL) {
                log_line("not enough memory to allocate document response");
                write_header(conn, STATUS_SERVER_UNAVAILABLE,
                    out_of_memory_msg);
                return;
            }
            size_t len = snprintf(entry, entrysize, "=>/docs/%s %s\r\n",
                docfiles[i].name, docfiles[i].title);
            assert(len == entrysize-1);
            if (write_tls(conn, entry, len) == -1) {
                free(entry);
                return;
            }
            free(entry);
        }

        write_tls(conn, footer, sizeof(footer)-1);
    } else if (strcmp(req->path, "/search") == 0) {
        if (req->query == NULL) {
            write_header(conn, STATUS_INPUT, "Please input the domain to view");
        } else {
            struct uri uri;
            if (!parse_uri(&uri, req->query)) {
                write_header(conn, STATUS_PERMANENT_FAILURE, "URI is invalid");
                return;
            }
            list_archive(conn, &uri);
            free_uri(&uri);
        }
    } else if (has_prefix(req->path, "/view/")) {
        view_archive(conn, req);
    } else if (strcmp(req->path, "/archive") == 0) {
        if (req->query == NULL) {
            write_header(conn, STATUS_INPUT, "Please input the URI to archive");
        } else {
            struct uri uri;
            if (!parse_uri(&uri, req->query)) {
                write_header(conn, STATUS_PERMANENT_FAILURE, "URI is invalid");
                return;
            }
            if (!invoke_crawler(conn, &uri)) {
                free_uri(&uri);
                return;
            }
            if (!write_header(conn, STATUS_SUCCESS, gemini_response)) {
                free_uri(&uri);
                return;
            }
            char *query = escape(req->query, QUERY_ESCAPE_CHARS);
            if (query == NULL) {
                log_line("not enough memory to allocate escaped query");
                free_uri(&uri);
                return;
            }

            size_t resp_len = sizeof(archive_success) + strlen(uri.domain)
                + strlen(query);
            char *resp = malloc(resp_len);
            if (resp == NULL) {
                log_line("not enough memory to allocate archive response");
            } else {
                size_t len = snprintf(resp, resp_len, archive_success,
                    uri.domain, query);
                assert(len < resp_len);
                write_tls(conn, resp, len);
                free(resp);
            }
            free(query);
            free_uri(&uri);
        }
    } else if (has_prefix(req->path, "/docs/")) {
        view_document(conn, &req->path[sizeof("/docs/") - 1]);
    } else {
        write_header(conn, STATUS_NOT_FOUND, "Resounce doesn't exist");
    }
}

struct rate_limit {
    double allowance;
    double last;
    uint64_t ip;
};

static time_t check_rate(struct sockaddr *sa) {
    uint64_t ip;
    char const *rate_file;
    if (sa->sa_family == AF_INET) {
        struct sockaddr_in *v4 = (struct sockaddr_in *)sa;
        ip = v4->sin_addr.s_addr;
        rate_file = "/nostalgia-rate-limit";
    } else {
        struct sockaddr_in6 *v6 = (struct sockaddr_in6 *)sa;

        // NOTE: since IPv6 interface ID can easily be changed on the fly, we
        // ignore that section of an IPv6 address to prevent bypassing the rate
        // limiting system.
        ip = *(uint64_t *)&v6->sin6_addr.s6_addr;
        rate_file = "/nostalgia-rate-limit-v6";
    }

    int fd = shm_open(rate_file, O_RDWR | O_CREAT, 0700);
    if (fd == -1) {
        log_format("failed to open rate limit file: %s", strerror(errno));
        return -1;
    }

    if (flock(fd, LOCK_EX) == -1) {
        log_format("failed to lock rate limit file: %s", strerror(errno));
        close(fd);
        return -1;
    }

    struct timespec ts;
    int status = clock_gettime(CLOCK_MONOTONIC, &ts);
    double t = ts.tv_sec + ts.tv_nsec / 1000000000.0;
    assert(status != -1);

    for (;;) {
        struct rate_limit entry;
        ssize_t count = read(fd, &entry, sizeof(entry));
        if (count == -1) {
            log_format("failed to read rate limit file: %s", strerror(errno));
            close(fd);
            return -1;
        } else if (count < sizeof(entry)) {
            break;
        }
        if (ip == entry.ip) {
            size_t passed = t - entry.last;
            entry.allowance += passed * (RATE / PER);
            if (entry.allowance > RATE) {
                entry.allowance = RATE;
            }
            entry.allowance -= 1.0;
            entry.last = t;

            if (lseek(fd, -sizeof(entry), SEEK_CUR) == -1) {
                log_format("failed to seek rate limit file: %s",
                    strerror(errno));
                close(fd);
                return -1;
            }

            if (write(fd, &entry, sizeof(entry)) == -1) {
                log_format("failed to write rate limit file: %s",
                    strerror(errno));
                close(fd);
                return -1;
            }
            close(fd);
            if (entry.allowance >= 0.0) {
                return 0;
            }
            return (time_t)(-entry.allowance / (RATE / PER)) + 1;
        }
    }

    struct rate_limit entry = {
        .ip = ip,
        .allowance = RATE - 1.0,
        .last = t,
    };
    if (write(fd, &entry, sizeof(entry)) == -1) {
        log_format("failed to write rate limit file: %s", strerror(errno));
        close(fd);
        return -1;
    }
    close(fd);
    return 0;
}

void handle_client(int socket) {
    union {
        struct sockaddr sa;
        struct sockaddr_in v4;
        struct sockaddr_in6 v6;
    } sa;
    socklen_t addrlen = sizeof(sa);
    if (getpeername(socket, &sa.sa, &addrlen) == -1) {
        log_format("failed to get peer address: %s", errno_string(errno));
        return;
    }
    char addr[INET6_ADDRSTRLEN];
    if (sa.sa.sa_family == AF_INET) {
        char const *a = inet_ntop(AF_INET, &sa.v4.sin_addr, addr, sizeof(addr));
        assert(a != NULL);
    } else {
        char const *a = inet_ntop(AF_INET6, &sa.v6.sin6_addr, addr,
            sizeof(addr));
        assert(a != NULL);
    }

    time_t rate = check_rate(&sa.sa);
    if (rate == -1) {
        return;
    }

    void *conn = open_tls(socket, 5000);
    if (conn == NULL) {
        return;
    }

    if (rate != 0) {
        set_tls_timeout(conn, 60000);
        log_format("(%s) THROTTLE %zu", addr, (size_t)rate);

        char rate_str[21];
        size_t count = snprintf(rate_str, sizeof(rate_str), "%zu",
            (size_t)rate);
        assert(count < sizeof(rate_str));
        write_header(conn, STATUS_SLOW_DOWN, rate_str);
        close_tls(conn);
        return;
    }

    char *linebuf = malloc(LINE_MAX * sizeof(char));
    if (linebuf == NULL) {
        log_line("not enough memory to allocate line buffer");
        close_tls(conn);
        return;
    }
    set_tls_timeout(conn, 15000);
    if (read_line(conn, linebuf, LINE_MAX * sizeof(char)) == -1) {
        free(linebuf);
        close_tls(conn);
        return;
    }

    log_format("(%s) ACCEPT %s", addr, linebuf);

    set_tls_timeout(conn, 60000);
    struct uri req;
    if (!parse_uri(&req, linebuf)) {
        write_header(conn, STATUS_BAD_REQUEST, "Received malformatted URI");
        free(linebuf);
        close_tls(conn);
        return;
    }

    handle_path(conn, &req);

    free_uri(&req);
    free(linebuf);
    close_tls(conn);
}
