/*
    Copyright (C) 2022  Gustaf "Hanicef" Alhäll

    This file is part of Nostalgia Project.

    Nostalgia Project is free software: you can redistribute it and/or modify it
    under the terms of the GNU Affero General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    Nostalgia Project is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Nostalgia Project. If not, see <https://www.gnu.org/licenses/>.
*/

#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include <gnutls/abstract.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <pthread.h>

#include "tls.h"
#include "utils.h"

#define EXPIRE_TIME 86400 * 30

static gnutls_certificate_credentials_t client_cred;
static gnutls_certificate_credentials_t server_cred;
static gnutls_priority_t priority;

static void log_error(char const *s, int sock, int err) {
    union {
        struct sockaddr sa;
        struct sockaddr_in v4;
        struct sockaddr_in6 v6;
    } sa;
    socklen_t addrlen = sizeof(sa);
    if (getpeername(sock, &sa.sa, &addrlen) == -1) {
        log_format("%s: %s", s, gnutls_strerror(err));
    } else {
        char addr[INET6_ADDRSTRLEN];
        char const *a;
        if (sa.sa.sa_family == AF_INET) {
            a = inet_ntop(AF_INET, &sa.v4.sin_addr, addr, sizeof(addr));
        } else {
            a = inet_ntop(AF_INET6, &sa.v6.sin6_addr, addr, sizeof(addr));
        }
        assert(a != NULL);
        log_format("(%s) %s: %s", a, s, gnutls_strerror(err));
    }
}

static int verify_cert(gnutls_session_t session) {
    char const *hostname = gnutls_session_get_ptr(session);
    gnutls_certificate_type_t type = gnutls_certificate_type_get2(session,
        GNUTLS_CTYPE_CLIENT);

    int sock = gnutls_transport_get_int(session);
    union {
        struct sockaddr sa;
        struct sockaddr_in v4;
        struct sockaddr_in6 v6;
    } sa;
    socklen_t addrlen = sizeof(sa);
    char addr[INET6_ADDRSTRLEN];
    if (getpeername(sock, &sa.sa, &addrlen) == -1) {
        addr[0] = '\0';
    } else {
        if (sa.sa.sa_family == AF_INET) {
            char const *a = inet_ntop(AF_INET, &sa.v4.sin_addr, addr,
                sizeof(addr));
            assert(a != NULL);
        } else {
            char const *a = inet_ntop(AF_INET6, &sa.v6.sin6_addr, addr,
                sizeof(addr));
            assert(a != NULL);
        }
    }

    unsigned int cert_count;
    gnutls_datum_t const *cert_data = gnutls_certificate_get_peers(session,
        &cert_count);
    if (cert_data == NULL) {
        log_format("(%s) failed to retrieve certificate peers", addr);
        return GNUTLS_E_CERTIFICATE_ERROR;
    }

    gnutls_x509_crt_t *certs;
    int err = gnutls_x509_crt_list_import2(&certs, &cert_count, cert_data,
        GNUTLS_X509_FMT_DER, 0);
    if (err < 0) {
        log_error("failed to parse certificate", sock, err);
        return GNUTLS_E_CERTIFICATE_ERROR;
    }

    size_t size = 32;
    uint8_t *fingerprint = malloc(32);
    if (fingerprint == NULL) {
        log_line("failed to allocate buffer for fingerprint");
        for (size_t i = 0; i < cert_count; i++) {
            gnutls_x509_crt_deinit(certs[i]);
        }
        gnutls_free(certs);
        return GNUTLS_E_MEMORY_ERROR;
    }

    err = gnutls_x509_crt_get_fingerprint(certs[0], GNUTLS_DIG_SHA1,
        fingerprint, &size);
    if (err < 0) {
        log_error("failed to retrieve certificate fingerprint", sock, err);
        free(fingerprint);
        for (size_t i = 0; i < cert_count; i++) {
            gnutls_x509_crt_deinit(certs[i]);
        }
        gnutls_free(certs);
        return GNUTLS_E_CERTIFICATE_ERROR;
    }

    char *buf = malloc(sizeof(char) * (size * 2 + 1));
    if (buf == NULL) {
        log_line("failed to allocate buffer for fingerprint");
        free(fingerprint);
        for (size_t i = 0; i < cert_count; i++) {
            gnutls_x509_crt_deinit(certs[i]);
        }
        gnutls_free(certs);
        return GNUTLS_E_MEMORY_ERROR;
    }
    for (size_t i = 0; i < size; i++) {
        uint8_t high = fingerprint[i] >> 4, low = fingerprint[i] & 0xf;
        buf[i*2] = (high < 10 ? high + '0' : high + 'a' - 10);
        buf[i*2 + 1] = (low < 10 ? low + '0' : low + 'a' - 10);
    }
    buf[size*2] = '\0';
    free(fingerprint);

    err = gnutls_verify_stored_pubkey(NULL, NULL, hostname, "gemini", type,
        &cert_data[0], 0);
    switch (err) {
        case GNUTLS_E_NO_CERTIFICATE_FOUND:
            log_format("(%s) accepting new certificate from %s; "
                "fingerprint is %s", addr, hostname, buf);
            free(buf);

            time_t expire = gnutls_x509_crt_get_expiration_time(certs[0]);
            if (expire == -1) {
                log_format("(%s) certificate has an invalid expiration time",
                    addr);
                for (size_t i = 0; i < cert_count; i++) {
                    gnutls_x509_crt_deinit(certs[i]);
                }
                gnutls_free(certs);
                return GNUTLS_E_CERTIFICATE_ERROR;
            }

            err = gnutls_store_pubkey(NULL, NULL, hostname, "gemini", type,
                &cert_data[0], expire, 0);
            if (err < 0) {
                log_error("failed to store new certificate", sock, err);
                for (size_t i = 0; i < cert_count; i++) {
                    gnutls_x509_crt_deinit(certs[i]);
                }
                gnutls_free(certs);
                return GNUTLS_E_CERTIFICATE_ERROR;
            }
            break;

        case GNUTLS_E_CERTIFICATE_KEY_MISMATCH:
            log_format("(%s) certificate rejected, key has changed: %s", addr,
                buf);
            free(buf);
            for (size_t i = 0; i < cert_count; i++) {
                gnutls_x509_crt_deinit(certs[i]);
            }
            gnutls_free(certs);
            return GNUTLS_E_CERTIFICATE_ERROR;

        case GNUTLS_E_SUCCESS:
            // certificate is known and valid.
            free(buf);
            break;

        default:
            free(buf);
            for (size_t i = 0; i < cert_count; i++) {
                gnutls_x509_crt_deinit(certs[i]);
            }
            gnutls_free(certs);
            return err;
    }

    for (size_t i = 0; i < cert_count; i++) {
        gnutls_x509_crt_deinit(certs[i]);
    }
    gnutls_free(certs);
    return GNUTLS_E_SUCCESS;
}

static char const *cert_file;
static char const *hostname;

static gnutls_pcert_st certificate;
static pthread_mutex_t *cert_mutex;

static bool get_random_u64(uint64_t *v) {
    int fd = open("/dev/urandom", O_RDONLY);
    if (fd == -1) {
        return false;
    }
    if (read(fd, v, sizeof(v)) == -1) {
        return false;
    }
    close(fd);
    return true;
}

static int generate_cert(char const *cert) {
    int err = pthread_mutex_lock(cert_mutex);
    if (err != 0) {
        return GNUTLS_E_FILE_ERROR;
    }

    // check that certificate was not recently updated, to avoid race conditions
    // between requests.
    struct stat st;
    if (stat(cert, &st) == -1) {
        if (errno != ENOENT) {
            pthread_mutex_unlock(cert_mutex);
            return GNUTLS_E_FILE_ERROR;
        }
    } else if (st.st_atime + 60 >= time(NULL)) {
        // certificate was renewed in the last minute, don't renew again.
        pthread_mutex_unlock(cert_mutex);
        return GNUTLS_E_SUCCESS;
    }

    gnutls_x509_privkey_t privkey;
    err = gnutls_x509_privkey_init(&privkey);
    if (err < 0) {
        pthread_mutex_unlock(cert_mutex);
        return err;
    }

    gnutls_pk_algorithm_t algo = GNUTLS_PK_ECDSA;
    int bits = gnutls_sec_param_to_pk_bits(algo, GNUTLS_SEC_PARAM_HIGH);
    assert(bits > 0);
    err = gnutls_x509_privkey_generate(privkey, algo, bits, 0);
    if (err < 0) {
        gnutls_x509_privkey_deinit(privkey);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }

    err = gnutls_x509_privkey_verify_params(privkey);
    if (err < 0) {
        gnutls_x509_privkey_deinit(privkey);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }

    gnutls_datum_t priv_datum;
    err = gnutls_x509_privkey_export2(privkey, GNUTLS_X509_FMT_PEM,
        &priv_datum);
    if (err < 0) {
        gnutls_x509_privkey_deinit(privkey);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }

    gnutls_privkey_t pkey;
    err = gnutls_privkey_init(&pkey);
    if (err < 0) {
        gnutls_x509_privkey_deinit(privkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }
    err = gnutls_privkey_import_x509(pkey, privkey,
        GNUTLS_PRIVKEY_IMPORT_AUTO_RELEASE);
    if (err < 0) {
        gnutls_x509_privkey_deinit(privkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }

    gnutls_pubkey_t pubkey;
    err = gnutls_pubkey_init(&pubkey);
    if (err < 0) {
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }
    err = gnutls_pubkey_import_privkey(pubkey, pkey, 0, 0);
    if (err < 0) {
        gnutls_pubkey_deinit(pubkey);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }
    gnutls_digest_algorithm_t digest;
    err = gnutls_pubkey_get_preferred_hash_algorithm(pubkey, &digest, NULL);
    if (err < 0) {
        gnutls_pubkey_deinit(pubkey);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }

    gnutls_x509_crt_t crt;
    err = gnutls_x509_crt_init(&crt);
    if (err < 0) {
        gnutls_pubkey_deinit(pubkey);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }

    err = gnutls_x509_crt_set_pubkey(crt, pubkey);
    if (err < 0) {
        gnutls_x509_crt_deinit(crt);
        gnutls_pubkey_deinit(pubkey);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }
    gnutls_pubkey_deinit(pubkey);

    err = gnutls_x509_crt_set_version(crt, 3);
    if (err < 0) {
        gnutls_x509_crt_deinit(crt);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }

    time_t now = time(NULL);
    err = gnutls_x509_crt_set_activation_time(crt, now);
    if (err < 0) {
        gnutls_x509_crt_deinit(crt);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }

    err = gnutls_x509_crt_set_expiration_time(crt, now + EXPIRE_TIME);
    if (err < 0) {
        gnutls_x509_crt_deinit(crt);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }

    char *dn = malloc(sizeof("CN=") + strlen(hostname));
    if (dn == NULL) {
        gnutls_x509_crt_deinit(crt);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }
    strcpy(dn, "CN=");
    strcat(dn, hostname);
    err = gnutls_x509_crt_set_dn(crt, dn, NULL);
    if (err < 0) {
        free(dn);
        gnutls_x509_crt_deinit(crt);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }
    err = gnutls_x509_crt_set_issuer_dn(crt, dn, NULL);
    if (err < 0) {
        free(dn);
        gnutls_x509_crt_deinit(crt);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }
    free(dn);

    uint64_t serial;
    if (!get_random_u64(&serial)) {
        gnutls_x509_crt_deinit(crt);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return GNUTLS_E_FILE_ERROR;
    }
    char serial_str[17];
    err = gnutls_x509_crt_set_serial(crt, format_hex_string(serial_str, serial),
        16);
    if (err < 0) {
        gnutls_x509_crt_deinit(crt);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }

    err = gnutls_x509_crt_set_key_usage(crt, GNUTLS_KEY_DIGITAL_SIGNATURE);
    if (err < 0) {
        gnutls_x509_crt_deinit(crt);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }

    err = gnutls_x509_crt_privkey_sign(crt, crt, pkey, digest, 0);
    if (err < 0) {
        gnutls_x509_crt_deinit(crt);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }
    gnutls_datum_t cert_datum;
    err = gnutls_x509_crt_export2(crt, GNUTLS_X509_FMT_PEM, &cert_datum);
    if (err < 0) {
        gnutls_x509_crt_deinit(crt);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }
    gnutls_x509_crt_deinit(crt);

    gnutls_pcert_st pcert;
    err = gnutls_pcert_import_x509_raw(&pcert, &cert_datum, GNUTLS_X509_FMT_PEM,
        0);
    if (err < 0) {
        gnutls_privkey_deinit(pkey);
        gnutls_free(cert_datum.data);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return err;
    }

    char *s = malloc(strlen(cert) + sizeof(".tmp"));
    if (s == NULL) {
        gnutls_pcert_deinit(&pcert);
        gnutls_privkey_deinit(pkey);
        gnutls_free(cert_datum.data);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return GNUTLS_E_MEMORY_ERROR;
    }

    strcpy(s, cert);
    strcat(s, ".tmp");
    int fd = open(s, O_WRONLY | O_CREAT | O_TRUNC, 0600);
    if (fd == -1) {
        free(s);
        gnutls_pcert_deinit(&pcert);
        gnutls_privkey_deinit(pkey);
        gnutls_free(cert_datum.data);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return GNUTLS_E_FILE_ERROR;
    }

    if (write(fd, cert_datum.data, cert_datum.size) == -1) {
        close(fd);
        unlink(s);
        free(s);
        gnutls_pcert_deinit(&pcert);
        gnutls_privkey_deinit(pkey);
        gnutls_free(cert_datum.data);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return GNUTLS_E_FILE_ERROR;
    }
    gnutls_free(cert_datum.data);
    if (write(fd, priv_datum.data, priv_datum.size) == -1) {
        close(fd);
        unlink(s);
        free(s);
        gnutls_pcert_deinit(&pcert);
        gnutls_privkey_deinit(pkey);
        gnutls_free(priv_datum.data);
        pthread_mutex_unlock(cert_mutex);
        return GNUTLS_E_FILE_ERROR;
    }
    gnutls_free(priv_datum.data);
    close(fd);
    if (rename(s, cert) == -1) {
        unlink(s);
        free(s);
        gnutls_pcert_deinit(&pcert);
        gnutls_privkey_deinit(pkey);
        pthread_mutex_unlock(cert_mutex);
        return GNUTLS_E_FILE_ERROR;
    }

    gnutls_pcert_deinit(&pcert);
    gnutls_privkey_deinit(pkey);
    log_format("generated new certificate, serial %s", serial_str);
    pthread_mutex_unlock(cert_mutex);
    return GNUTLS_E_SUCCESS;
}

static int get_cert(gnutls_session_t session, gnutls_datum_t const *req_ca_dn,
        int nreqs, gnutls_pk_algorithm_t const *pk_algos, int pk_algos_length,
        gnutls_pcert_st **pcert, unsigned int *pcert_length,
        gnutls_privkey_t *pkey) {
    int fd = open(cert_file, O_RDONLY);
    if (fd == -1) {
        if (errno == ENOENT) {
            int err = generate_cert(cert_file);
            if (err < 0) {
                log_format("failed to generate certificate: %s",
                    gnutls_strerror(err));
                return false;
            }
            return get_cert(session, req_ca_dn, nreqs, pk_algos,
                pk_algos_length, pcert, pcert_length, pkey);
        }
        log_format("failed to open certificate file: %s", strerror(errno));
        return GNUTLS_E_FILE_ERROR;
    }

    struct stat st;
    if (fstat(fd, &st) == -1) {
        log_format("failed to stat certificate: %s", strerror(errno));
        close(fd);
        return GNUTLS_E_FILE_ERROR;
    }
    gnutls_datum_t datum = {
        .data = gnutls_malloc(st.st_size),
        .size = st.st_size,
    };
    if (datum.data == NULL) {
        log_format("not enough memory to allocate certificate buffer");
        close(fd);
        return GNUTLS_E_MEMORY_ERROR;
    }
    if (read(fd, datum.data, datum.size) == -1) {
        log_format("failed to read certificate file: %s", strerror(errno));
        close(fd);
        gnutls_free(datum.data);
        return GNUTLS_E_FILE_ERROR;
    }
    close(fd);

    int err = gnutls_privkey_init(pkey);
    if (err < 0) {
        log_line("not enough memory to allocate private key");
        gnutls_free(datum.data);
        return err;
    }
    err = gnutls_privkey_import_x509_raw(*pkey, &datum, GNUTLS_X509_FMT_PEM,
        NULL, 0);
    if (err < 0) {
        log_format("failed to import private key: %s", gnutls_strerror(err));
        gnutls_free(datum.data);
        gnutls_privkey_deinit(*pkey);
        return err;
    }

    *pcert_length = 1;
    *pcert = &certificate;
    err = gnutls_pcert_import_x509_raw(*pcert, &datum, GNUTLS_X509_FMT_PEM, 0);
    if (err < 0) {
        log_format("failed to import certificate: %s", gnutls_strerror(err));
        gnutls_free(datum.data);
        gnutls_privkey_deinit(*pkey);
        return err;
    }

    gnutls_x509_crt_t crt;
    err = gnutls_x509_crt_init(&crt);
    if (err < 0) {
        log_line("not enough memory to allocate x509 certificate");
        gnutls_free(datum.data);
        gnutls_pcert_deinit(*pcert);
        gnutls_privkey_deinit(*pkey);
        return err;
    }
    err = gnutls_x509_crt_import(crt, &datum, GNUTLS_X509_FMT_PEM);
    if (err < 0) {
        log_format("failed to import x509 certificate: %s",
            gnutls_strerror(err));
        gnutls_free(datum.data);
        gnutls_pcert_deinit(*pcert);
        gnutls_privkey_deinit(*pkey);
        return err;
    }
    gnutls_free(datum.data);

    time_t expire = gnutls_x509_crt_get_expiration_time(crt);
    if (expire < time(NULL)) {
        gnutls_x509_crt_deinit(crt);
        err = generate_cert(cert_file);
        if (err < 0) {
            log_format("failed to generate certificate: %s",
                gnutls_strerror(err));
            return false;
        }
        return get_cert(session, req_ca_dn, nreqs, pk_algos, pk_algos_length,
            pcert, pcert_length, pkey);
    }

    gnutls_x509_crt_deinit(crt);
    return GNUTLS_E_SUCCESS;
}

static bool create_mutex(void) {
    static char const prefix[] = "/nostalgia-mutex-";

    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    int err = pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);
    if (err != 0) {
        log_format("failed to create cert mutex: %s", strerror(err));
        return false;
    }

    int fd = open("/dev/urandom", O_RDONLY);
    if (fd == -1) {
        log_format("failed to open /dev/urandom: %s", strerror(errno));
        return false;
    }
    char buf[6];
    if (read(fd, buf, sizeof(buf)) == -1) {
        log_format("failed to read /dev/urandom: %s", strerror(errno));
        close(fd);
        return false;
    }
    close(fd);
    char path[sizeof(prefix) + 8];
    strcpy(path, prefix);
    format_base64(&path[sizeof(prefix)-1], 9, buf, sizeof(buf));

    fd = shm_open(path, O_RDWR | O_CREAT | O_TRUNC, 0700);
    if (fd == -1) {
        log_format("failed to create shared memory: %s", strerror(errno));
        return false;
    }
    if (ftruncate(fd, sizeof(pthread_mutex_t)) == -1) {
        log_format("failed to expand shared memory: %s", strerror(errno));
        close(fd);
        unlink(path);
        return false;
    }
    cert_mutex = mmap(NULL, sizeof(pthread_mutex_t), PROT_READ | PROT_WRITE,
        MAP_SHARED, fd, 0);
    if (cert_mutex == MAP_FAILED) {
        log_format("failed to map shared memory: %s", strerror(errno));
        close(fd);
        unlink(path);
        return false;
    }
    close(fd);
    unlink(path);

    pthread_mutex_init(cert_mutex, &attr);
    return true;
}

bool init_tls(char const *cert, char const *host) {
    int err = gnutls_global_init();
    if (err < 0) {
        log_format("failed to initialize gnutls: %s", gnutls_strerror(err));
        return false;
    }

    err = gnutls_certificate_allocate_credentials(&server_cred);
    if (err < 0) {
        log_format("failed to allocate credentials: %s",
            gnutls_strerror(err));
        return false;
    }

    err = gnutls_certificate_allocate_credentials(&client_cred);
    if (err < 0) {
        log_format("failed to allocate credentials: %s",
            gnutls_strerror(err));
        gnutls_certificate_free_credentials(server_cred);
        return false;
    }

    cert_file = cert;
    hostname = host;
    gnutls_certificate_set_retrieve_function2(server_cred, get_cert);
    gnutls_certificate_set_verify_function(client_cred, verify_cert);

    err = gnutls_priority_init(&priority, NULL, NULL);
    if (err < 0) {
        log_format("failed to create priority: %s", gnutls_strerror(err));
        gnutls_certificate_free_credentials(server_cred);
        gnutls_certificate_free_credentials(client_cred);
        return false;
    }

    if (!create_mutex()) {
        gnutls_certificate_free_credentials(server_cred);
        gnutls_certificate_free_credentials(client_cred);
        return false;
    }
    return true;
}

void *open_tls(int sock, unsigned int timeout) {
    gnutls_session_t session;
    int err = gnutls_init(&session, GNUTLS_SERVER);
    if (err < 0) {
        log_error("failed to create gnutls session", sock, err);
        return NULL;
    }
    err = gnutls_priority_set(session, priority);
    if (err < 0) {
        log_error("failed to set session priority", sock, err);
        gnutls_deinit(session);
        return NULL;
    }
    err = gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, server_cred);
    if (err < 0) {
        log_error("failed to set credentials", sock, err);
        gnutls_deinit(session);
        return NULL;
    }

    gnutls_certificate_server_set_request(session, GNUTLS_CERT_IGNORE);
    gnutls_transport_set_pull_timeout_function(session,
        gnutls_system_recv_timeout);
    gnutls_handshake_set_timeout(session, timeout);
    gnutls_transport_set_int(session, sock);

    err = gnutls_handshake(session);
    if (err < 0) {
        log_error("failed to perform handshake", sock, err);
        gnutls_deinit(session);
        return NULL;
    }

    return (void *)session;
}

void *connect_tls(int sock, char const *domain, unsigned int timeout) {
    gnutls_session_t session;
    int err = gnutls_init(&session, GNUTLS_CLIENT);
    if (err < 0) {
        log_error("failed to create gnutls session", sock, err);
        return NULL;
    }

    err = gnutls_priority_set(session, priority);
    if (err < 0) {
        log_error("failed to set session priority", sock, err);
        gnutls_deinit(session);
        return NULL;
    }
    err = gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, client_cred);
    if (err < 0) {
        log_error("failed to set credentials", sock, err);
        gnutls_deinit(session);
        return NULL;
    }
    err = gnutls_server_name_set(session, GNUTLS_NAME_DNS, domain,
        strlen(domain));
    if (err < 0) {
        log_error("failed to set server name", sock, err);
        gnutls_deinit(session);
        return NULL;
    }
    gnutls_session_set_ptr(session, domain);

    gnutls_transport_set_int(session, sock);
    gnutls_transport_set_pull_timeout_function(session,
        gnutls_system_recv_timeout);
    gnutls_handshake_set_timeout(session, timeout);

    err = gnutls_handshake(session);
    if (err < 0) {
        log_error("failed to perform handshake", sock, err);
        gnutls_deinit(session);
        return NULL;
    }
    return (void *)session;
}

void close_tls(void *conn) {
    gnutls_session_t session = (gnutls_session_t)conn;
    gnutls_bye(session, GNUTLS_SHUT_WR);
    gnutls_deinit((gnutls_session_t)conn);
}

void set_tls_timeout(void *conn, unsigned int ms) {
    gnutls_session_t session = (gnutls_session_t)conn;
    gnutls_record_set_timeout(session, ms);
}

ssize_t read_tls(void *conn, void *buf, size_t count) {
    gnutls_session_t session = (gnutls_session_t)conn;
    ssize_t n = gnutls_record_recv(session, buf, count);
    if (n < 0) {
        if (n == GNUTLS_E_AGAIN || n == GNUTLS_E_INTERRUPTED) {
            return read_tls(conn, buf, count);
        } else if (n == GNUTLS_E_PREMATURE_TERMINATION) {
            // assume that the request is complete
            return 0;
        }

        log_error("read error", gnutls_transport_get_int(session), n);
        return -1;
    }
    return n;
}

ssize_t write_tls(void *conn, void const *buf, size_t count) {
    gnutls_session_t session = (gnutls_session_t)conn;
    ssize_t n = gnutls_record_send(session, buf, count);
    if (n < 0) {
        if (n == GNUTLS_E_AGAIN || n == GNUTLS_E_INTERRUPTED) {
            return write_tls(conn, buf, count);
        }

        log_error("write error", gnutls_transport_get_int(session), n);
        return -1;
    }
    return n;
}
