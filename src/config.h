/*
    Copyright (C) 2022  Gustaf "Hanicef" Alhäll

    This file is part of Nostalgia Project.

    Nostalgia Project is free software: you can redistribute it and/or modify it
    under the terms of the GNU Affero General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    Nostalgia Project is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Nostalgia Project. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONFIG_H
#define CONFIG_H

#include <stdbool.h>

struct config {
    struct {
        char const *address;
        int port;
        char const *host;
    } server;

    struct {
        char const *certificate;
    } tls;

    struct {
        char const *archive_path;
        size_t file_size_limit;
        char const *blacklist_path;
        size_t archive_interval;
    } archiver;

    struct {
        char const *doc_path;
    } misc;
};

extern struct config config;

bool load_config(char const *path);

#endif // CONFIG_H
