/*
    Copyright (C) 2022  Gustaf "Hanicef" Alhäll

    This file is part of Nostalgia Project.

    Nostalgia Project is free software: you can redistribute it and/or modify it
    under the terms of the GNU Affero General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    Nostalgia Project is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Nostalgia Project. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CLIENT_H
#define CLIENT_H

#include <netinet/in.h>

#include "config.h"

enum {
    STATUS_INPUT = 10,
    STATUS_SENSITIVE_INPUT = 11,

    STATUS_SUCCESS = 20,

    STATUS_REDIRECT_TEMPORARY = 30,
    STATUS_REDIRECT_PERMANENT = 31,

    STATUS_TEMPORARY_FAILURE = 40,
    STATUS_SERVER_UNAVAILABLE = 41,
    STATUS_CGI_ERROR = 42,
    STATUS_PROXY_ERROR = 43,
    STATUS_SLOW_DOWN = 44,

    STATUS_PERMANENT_FAILURE = 50,
    STATUS_NOT_FOUND = 51,
    STATUS_GONE = 52,
    STATUS_PROXY_REQUEST_REFUSED = 53,
    STATUS_BAD_REQUEST = 59,

    STATUS_CLIENT_CERTIFICATE_REQUIRED = 60,
    STATUS_CERTIFICATE_NOT_AUTHORIZED = 61,
    STATUS_CERTIFICATE_NOT_VALID = 62,
};

struct uri {
    char *domain;
    char *path;
    char *query;
    int port;
};

extern char const out_of_memory_msg[];

ssize_t read_line(void *conn, char *buf, size_t bufsize);
ssize_t read_file_line(int fd, char *buf, size_t bufsize);
bool write_header(void *conn, int status, char const *message);

bool parse_uri(struct uri *uri, char const *s);
char *format_uri(struct uri *uri);
void free_uri(struct uri *uri);

void handle_client(int socket);

#endif // CLIENT_H
