/*
    Copyright (C) 2022  Gustaf "Hanicef" Alhäll

    This file is part of Nostalgia Project.

    Nostalgia Project is free software: you can redistribute it and/or modify it
    under the terms of the GNU Affero General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    Nostalgia Project is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Nostalgia Project. If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <time.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <inttypes.h>
#include <ctype.h>
#include <strings.h>
#include <sys/mman.h>

#include "config.h"
#include "crawl.h"
#include "client.h"
#include "hash.h"
#include "tls.h"
#include "utils.h"
#include "storage.h"

#define LINE_MAX 1024
#define BLOCK_SIZE 1024

#define MAX_REDIRECTS 5
#define REQUEST_INTERVAL 2

struct allow_path {
    char *s;
    bool allow;
};

struct context {
    union {
        struct sockaddr sa;
        struct sockaddr_in in4;
        struct sockaddr_in6 in6;
    };

    struct uri *uri;
    struct allow_path *allow_list;
    struct timespec last_request;
    size_t allow_list_size;
    int redirect_count;
    bool next;
    char port_str[6];
};

static bool is_ipv4_sane(uint32_t addr) {
    // reference: https://www.iana.org/assignments/iana-ipv4-special-registry/iana-ipv4-special-registry.xhtml
    return ((addr & 0xff000000) != 0x00000000 && // 0.0.0.0/8
        (addr & 0xffffffff) != 0x00000000 && // 0.0.0.0/32
        (addr & 0xff000000) != 0x0a000000 && // 10.0.0.0/8
        (addr & 0xffc00000) != 0x64400000 && // 100.64.0.0/10
        (addr & 0xff000000) != 0x7f000000 && // 127.0.0.0/8
        (addr & 0xffff0000) != 0xa9fe0000 && // 169.254.0.0/16
        (addr & 0xfff00000) != 0xac100000 && // 172.16.0.0/12
        (addr & 0xffffff00) != 0xc0000000 && // 192.0.0.0/24
        (addr & 0xffffff00) != 0xc0000200 && // 192.0.2.0/24
        (addr & 0xffffff00) != 0xc01fc400 && // 192.31.196.0/24
        (addr & 0xffffff00) != 0xc034c100 && // 192.52.193.0/24
        (addr & 0xffffff00) != 0xc0586300 && // 192.88.99.0/24
        (addr & 0xffff0000) != 0xc0a80000 && // 192.168.0.0/16
        (addr & 0xfffe0000) != 0xc6120000 && // 198.18.0.0/15
        (addr & 0xffffff00) != 0xc6336400 && // 198.51.100.0/24
        (addr & 0xffffff00) != 0xcb007100 && // 203.0.113.0/24
        (addr & 0xf0000000) != 0xf0000000 && // 240.0.0.0/4
        (addr & 0xffffffff) != 0xffffffff); // 255.255.255.255/32
}

// ::1/128
static unsigned char v6_loopback[16] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1
};
// ::/128
static unsigned char v6_any[16] = { 0 };
// ::ffff:0:0/96
static unsigned char v6_v4mapped[12] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xff, 0xff
};
// 64:ff9b::/96
static unsigned char v6_v4translate[12] = {
    0, 0x64, 0xff, 0x9b, 0, 0, 0, 0, 0, 0, 0
};
// 64:ff9b:1::/48
static unsigned char v6_localtranslate[6] = { 0, 0x64, 0xff, 0x9b, 0, 1 }; 
// 100::/64
static unsigned char v6_discard[8] = { 1, 0, 0, 0, 0, 0, 0, 0 };

static bool is_ipv6_sane(unsigned char const addr[static 16]) {
    // reference: https://www.iana.org/assignments/iana-ipv6-special-registry/iana-ipv6-special-registry.xhtml
    if (memcmp(addr, v6_loopback, sizeof(v6_loopback)) == 0 ||
            memcmp(addr, v6_any, sizeof(v6_any)) == 0 ||
            memcmp(addr, v6_v4mapped, sizeof(v6_v4mapped)) == 0 ||
            memcmp(addr, v6_v4translate, sizeof(v6_v4translate)) == 0 ||
            memcmp(addr, v6_localtranslate, sizeof(v6_localtranslate)) == 0 ||
            memcmp(addr, v6_discard, sizeof(v6_discard)) == 0) {
        return false;
    }
    // 2001::/23
    if (addr[0] == 0x20 && addr[1] == 1 && (addr[2] & 0xfe) == 0) {
        return false;
    }
    // 2001:db8::/32
    if (addr[0] == 0x20 && addr[1] == 1 && addr[2] == 0xd && addr[3] == 0xb8) {
        return false;
    }
    // 2002::/16
    if (addr[0] == 0x20 && addr[1] == 2) {
        return false;
    }
    // fc00::/7
    if ((addr[0] & 0xfe) == 0xfc) {
        return false;
    }
    // fe80::/10
    if (addr[0] == 0xfe && (addr[1] & 0xc0) == 0x80) {
        return false;
    }
    // ff00::/8
    if (addr[0] == 0xff) {
        return false;
    }
    return true;
}

static bool resolve_domain(char const *domain, char const *port,
        struct sockaddr *sa) {
    struct addrinfo info = {
        .ai_flags = AI_ADDRCONFIG | AI_V4MAPPED | AI_NUMERICSERV,
        .ai_family = AF_UNSPEC,
        .ai_socktype = SOCK_STREAM,
        .ai_protocol = 0,
        .ai_addrlen = 0,
        .ai_addr = NULL,
        .ai_canonname = NULL,
        .ai_next = NULL,
    };
    struct addrinfo *res, *rp;
    int err = getaddrinfo(domain, port, &info, &res);
    if (err != 0) {
        if (err != EAI_NONAME) {
            log_format("failed to resolve address: %s", gai_strerror(err));
        }
        return false;
    }
    for (rp = res; rp != NULL; rp = rp->ai_next) {
        if (rp->ai_family == AF_INET) {
            struct sockaddr_in *addr = (struct sockaddr_in *)rp->ai_addr;
            if (is_ipv4_sane(ntohl(addr->sin_addr.s_addr))) {
                memcpy(sa, addr, sizeof(*addr));
                freeaddrinfo(res);
                return true;
            }
        } else {
            struct sockaddr_in6 *addr = (struct sockaddr_in6 *)rp->ai_addr;
            if (is_ipv6_sane(addr->sin6_addr.s6_addr)) {
                memcpy(sa, addr, sizeof(*addr));
                freeaddrinfo(res);
                return true;
            }
        }
    }
    freeaddrinfo(res);
    return false;
}

static bool is_path_allowed(struct context *context) {
    bool allowed = true;
    for (size_t i = 0; i < context->allow_list_size; i++) {
        if (has_prefix(context->uri->path, context->allow_list[i].s)) {
            allowed = context->allow_list[i].allow;
        }
    }
    return allowed;
}

static bool send_request(struct context *context,
        bool (*callback)(void *conn, struct context *context)) {
    context->next = true;
    while (context->next) {
        context->next = false;
        if (!is_path_allowed(context)) {
            log_format("refuses to archive %s%s: robots.txt disallows it",
                context->uri->domain, context->uri->path);
            return false;
        }

        char *uri = format_uri(context->uri);
        if (uri == NULL) {
            log_line("not enough memory to allocate request uri");
            return false;
        }
        char addr[INET6_ADDRSTRLEN];
        if (context->sa.sa_family == AF_INET) {
            char const *a = inet_ntop(AF_INET, &context->in4.sin_addr, addr,
                sizeof(addr));
            assert(a != NULL);
        } else {
            char const *a = inet_ntop(AF_INET6, &context->in6.sin6_addr, addr,
                sizeof(addr));
            assert(a != NULL);
        }
        log_format("(%s) ARCHIVE %s", addr, uri);

        char *req = realloc(uri, strlen(uri) + 3);
        if (req == NULL) {
            log_line("not enough memory to allocate request uri");
            return false;
        }
        strcat(req, "\r\n");

        int sock = socket(context->sa.sa_family, SOCK_STREAM, 0);
        if (sock == -1) {
            log_format("(%s) failed to open socket: %s", addr,
                errno_string(errno));
            free(req);
            return false;
        }

        struct timeval timeout = { .tv_sec = 5, .tv_usec = 0 };
        if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &timeout,
                sizeof(timeout)) == -1) {
            log_format("(%s) failed to open socket: %s", addr,
                errno_string(errno));
            free(req);
            close(sock);
            return false;
        }
        if (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &timeout,
                sizeof(timeout)) == -1) {
            log_format("(%s) failed to open socket: %s", addr,
                errno_string(errno));
            free(req);
            close(sock);
            return false;
        }

        size_t size;
        if (context->sa.sa_family == AF_INET) {
            size = sizeof(struct sockaddr_in);
        } else {
            assert(context->sa.sa_family == AF_INET6);
            size = sizeof(struct sockaddr_in6);
        }

        struct timespec ts;
        if (clock_gettime(CLOCK_MONOTONIC, &ts) == -1) {
            log_format("(%s) failed to get system time: %s", addr,
                errno_string(errno));
            free(req);
            close(sock);
            return false;
        }

        // wait at least REQUEST_INTERVAL seconds until next request to avoid
        // flooding the server.
        ts.tv_sec = context->last_request.tv_sec + REQUEST_INTERVAL - ts.tv_sec;
        ts.tv_nsec = context->last_request.tv_nsec - ts.tv_nsec;
        if (ts.tv_nsec < 0) {
            ts.tv_sec--;
            ts.tv_nsec += 1000000000;
        }
        if (ts.tv_sec >= 0) {
            do {
                errno = 0;
                if (nanosleep(&ts, &ts) == -1 && errno != EINTR) {
                    log_format("(%s) failed to sleep: %s", addr,
                        errno_string(errno));
                    free(req);
                    close(sock);
                    return false;
                }
            } while (errno == EINTR);
        }

        if (connect(sock, &context->sa, size)) {
            if (errno == EINPROGRESS) {
                // NOTE: we're setting timeout on socket level but TCP handshake
                // is not aborted due to how TCP works. because of this,
                // EINPROGRESS is actually a timeout.
                errno = ETIMEDOUT;
            }
            log_format("(%s) failed to connect to server: %s", addr,
                errno_string(errno));
            free(req);
            close(sock);
            return false;
        }

        if (clock_gettime(CLOCK_MONOTONIC, &context->last_request) == -1) {
            log_format("(%s) failed to get system time: %s", addr,
                errno_string(errno));
            free(req);
            close(sock);
            return false;
        }

        void *conn = connect_tls(sock, context->uri->domain, 5000);
        if (conn == NULL) {
            free(req);
            shutdown(sock, SHUT_RDWR);
            close(sock);
            return false;
        }


        set_tls_timeout(conn, 5000);
        if (write_tls(conn, req, strlen(req)) == -1) {
            free(req);
            close_tls(conn);
            shutdown(sock, SHUT_RDWR);
            close(sock);
            return false;
        }
        free(req);

        set_tls_timeout(conn, 15000);
        bool status = callback(conn, context);
        close_tls(conn);
        shutdown(sock, SHUT_RDWR);
        close(sock);
        if (!status) {
            assert(!context->next);
            return false;
        }
    }
    return true;
}

static bool write_content(void *conn, int fd, char digest[static HASH_WIDTH]) {
    struct hash *hash = hash_init();
    if (hash == NULL) {
        log_line("not enough memory to allocate hash buffer");
        return false;
    }
    char *buf = malloc(BLOCK_SIZE);
    if (buf == NULL) {
        log_line("not enough memory to allocate write buffer");
        free(hash);
        return false;
    }
    for (;;) {
        ssize_t count = read_tls(conn, buf, BLOCK_SIZE);
        if (count == -1) {
            free(buf);
            free(hash);
            return false;
        } else if (count == 0) {
            break;
        }

        if (write(fd, buf, count) == -1) {
            log_format("failed to store file content: %s", errno_string(errno));
            free(buf);
            free(hash);
            return false;
        }

        hash_insert(hash, buf, count);
    }

    hash_digest(hash, digest);
    free(buf);
    free(hash);
    return true;
}

static enum search_status entry_exists(time_t date,
        char const hash[static HASH_WIDTH], char const *ctype, void *data) {
    return (memcmp(data, hash, HASH_WIDTH) == 0 ? SEARCH_MATCH :
        SEARCH_NO_MATCH);
}

static bool write_redirect(char const *domain, char const *from,
        char const *to) {
    char *redirect_path = join_path(config.archiver.archive_path, domain,
        "redirects", NULL);
    if (redirect_path == NULL) {
        log_line("not enough memory to allocate redirect path");
        return false;
    }

    int fd = open(redirect_path, O_RDWR | O_CREAT, 0644);
    if (fd == -1) {
        log_format("failed to open redirect file: %s", errno_string(errno));
        free(redirect_path);
        return false;
    }
    errno = 0;
    if (has_redirect(fd, from, to)) {
        close(fd);
        return true;
    } else if (errno != 0) {
        log_format("failed to read from redirect file: %s",
            errno_string(errno));
        close(fd);
        return false;
    }
    free(redirect_path);
    if (!add_redirect(fd, from, to)) {
        log_format("failed to write to redirect file: %s", errno_string(errno));
        close(fd);
        return false;
    }
    close(fd);
    return true;
}

static bool handle_response(void *conn, struct context *context) {
    char *linebuf = malloc(LINE_MAX * sizeof(char));
    if (linebuf == NULL) {
        return false;
    }

    char addr[INET6_ADDRSTRLEN];
    if (context->sa.sa_family == AF_INET) {
        char const *a = inet_ntop(AF_INET, &context->in4.sin_addr, addr,
            sizeof(addr));
        assert(a != NULL);
    } else {
        char const *a = inet_ntop(AF_INET6, &context->in6.sin6_addr, addr,
            sizeof(addr));
        assert(a != NULL);
    }

    for (size_t retries = 0; retries < 5; retries++) {
        ssize_t len = read_line(conn, linebuf, LINE_MAX * sizeof(char));
        if (len == -1) {
            free(linebuf);
            return false;
        }
        if (len < 3) {
            log_format("(%s) invalid response: header too short", addr);
            free(linebuf);
            return false;
        }
        if (len >= LINE_MAX) {
            log_format("(%s) invalid response: header too long", addr);
            free(linebuf);
            return false;
        }
        if (linebuf[1] < '0' || linebuf[1] > '9' || linebuf[2] != ' ') {
            log_format("(%s) invalid response: malformatted header", addr);
            free(linebuf);
            return false;
        }
        for (size_t i = 3; i < (size_t)len; i++) {
            if (linebuf[i] < 0x20 || (unsigned char)linebuf[i] > 0x7f) {
                log_format("(%s) invalid response: header is not text", addr);
                free(linebuf);
                return false;
            }
        }
        switch (linebuf[0]) {
            case '1':
                // TODO: handle input response
                log_format("(%s) invalid response: input not supported", addr);
                free(linebuf);
                return false;

            case '2':
                char *path = join_path(config.archiver.archive_path,
                    "content", context->uri->domain, NULL);
                if (path == NULL) {
                    log_line("not enough memory to allocate draft path");
                    free(linebuf);
                    return false;
                }

                int fd = open(path, O_WRONLY | O_CREAT | O_TRUNC, 0644);
                if (fd == -1) {
                    log_format("failed to create draft file: %s",
                        errno_string(errno));
                    free(linebuf);
                    free(path);
                    return false;
                }
                char hash[HASH_WIDTH];
                if (!write_content(conn, fd, hash)) {
                    free(linebuf);
                    free(path);
                    close(fd);
                    return false;
                }
                char hash_str[45];
                size_t len = format_base64(hash_str, sizeof(hash_str), hash,
                    sizeof(hash));
                assert(len < sizeof(hash_str));
                close(fd);

                time_t date = time(NULL);
                if (context->uri->query != NULL) {
                    len = strlen(context->uri->path) +
                        strlen(context->uri->query) + 2;
                    struct string_buffer query_buf;
                    if (!init_string(&query_buf, len)) {
                        log_line("not enough memory to allocate query uri");
                        return false;
                    }
                    append_string(&query_buf, context->uri->path);
                    append_string(&query_buf, "?");
                    append_string(&query_buf, context->uri->query);
                    char *query = close_string(&query_buf);
                    if (!add_index(context->uri->domain, query, date,
                            &linebuf[3], hash)) {
                        log_format("failed to add index entry: %s",
                            errno_string(errno));
                        free(query);
                        free(linebuf);
                        unlink(path);
                        free(path);
                        return false;
                    }
                    free(query);
                } else {
                    if (!add_index(context->uri->domain, context->uri->path,
                            date, &linebuf[3], hash)) {
                        log_format("failed to add index entry: %s",
                            errno_string(errno));
                        free(linebuf);
                        unlink(path);
                        free(path);
                        return false;
                    }
                }

                char *dest_path = join_path(config.archiver.archive_path,
                    "content", hash_str, NULL);
                if (dest_path == NULL) {
                    log_line("not enough memory to allocate draft path");
                    unlink(path);
                    free(path);
                    free(linebuf);
                    return false;
                }
                if (rename(path, dest_path) == -1) {
                    log_format("failed to rename draft file: %s",
                        errno_string(errno));
                    unlink(path);
                    free(dest_path);
                    free(path);
                    free(linebuf);
                    return false;
                }
                free(path);
                free(dest_path);
                free(linebuf);
                return true;

            case '3':
                struct uri redirect;
                if (++context->redirect_count >= MAX_REDIRECTS) {
                    log_format("(%s) invalid response: too many redirect",
                        addr);
                    free(linebuf);
                    return false;
                }
                size_t i;
                for (i = 3; isalnum(linebuf[i]) || linebuf[i] == '+' ||
                    linebuf[i] == '-' || linebuf[i] == '.'; i++);
                if (linebuf[i] == ':') {
                    if (!parse_uri(&redirect, &linebuf[3])) {
                        log_format("(%s) invalid response: bad redirect uri",
                            addr);
                    }
                    if (strcmp(redirect.domain, context->uri->domain) != 0 ||
                            redirect.port != context->uri->port) {
                        // don't allow redirects to other domains to prevent
                        // bypassing domain locks.
                        log_format("(%s) invalid response: "
                            "refuses to redirect to different domain", addr);
                        free_uri(&redirect);
                        free(linebuf);
                        return false;
                    }

                    if (!write_redirect(context->uri->domain,
                            context->uri->path, redirect.path)) {
                        free_uri(&redirect);
                        free(linebuf);
                        return false;
                    }

                    free_uri(context->uri);
                    memcpy(context->uri, &redirect, sizeof(struct uri));
                    context->next = true;
                    free(linebuf);
                    return true;
                } else if (linebuf[3] == '/') {
                    if (!write_redirect(context->uri->domain,
                            context->uri->path, &linebuf[3])) {
                        free(linebuf);
                        return false;
                    }
                    char *path = malloc(sizeof(char) *
                        (strlen(&linebuf[3]) + 1));
                    if (path == NULL) {
                        log_line("not enough memory to allocate redirect path");
                        free(linebuf);
                        return false;
                    }
                    strcpy(path, &linebuf[3]);

                    free(context->uri->path);
                    context->uri->path = path;
                    context->next = true;
                    free(linebuf);
                    return true;
                } else {
                    char *end_path = strrchr(context->uri->path, '/');
                    char *path;
                    if (end_path != NULL) {
                        size_t path_len = end_path - context->uri->path;
                        size_t len = path_len + strlen(&linebuf[3]) + 1;
                        path = malloc(len * sizeof(char));
                        if (path == NULL) {
                            log_line(
                                "not enough memory to allocate redirect path");
                            free(linebuf);
                            return false;
                        }
                        memcpy(path, context->uri->path, path_len);
                        path[path_len] = '/';
                        strcpy(&path[path_len + 1], &linebuf[3]);
                    } else {
                        path = join_path("/", &linebuf[3], NULL);
                        if (path == NULL) {
                            log_line(
                                "not enough memory to allocate redirect path");
                            free(linebuf);
                            return false;
                        }
                    }

                    if (!write_redirect(context->uri->domain,
                            context->uri->path, path)) {
                        free(path);
                        free(linebuf);
                        return false;
                    }

                    free(context->uri->path);
                    context->uri->path = path;
                    context->next = true;
                    free(linebuf);
                    return true;
                }

            case '4':
                if (linebuf[1] == '4') {
                    char *end;
                    unsigned long seconds = strtoul(&linebuf[3], &end, 10);
                    if (end[0] != '\0') {
                        log_format("(%s) invalid response: "
                            "malformatted 44 response", addr);
                        free(linebuf);
                        return false;
                    }
                    if (seconds > 10) {
                        log_format("(%s) invalid response: "
                            "refusing to wait %lu seconds", addr, seconds);
                        free(linebuf);
                        return false;
                    }
                    do {
                        seconds = sleep(seconds);
                    } while (seconds > 0);
                    break;
                }
                // fallthrough
            case '5':
                log_format("(%s) invalid response: %s", addr, linebuf);
                free(linebuf);
                return false;

            case '6':
                log_format("(%s) invalid response: unable to authenticate",
                    addr);
                free(linebuf);
                return false;
            default:
                log_format("(%s) invalid response: unknown status code", addr);
                free(linebuf);
                return false;
        }
    }

    log_format("invalid response from %s: giving up after multiple tries",
        context->uri->domain);
    free(linebuf);
    return false;
}

static char const *user_agents[] = {
    "nostalgia-project",
    "archiver",
    "*",
};

static bool parse_robots(void *conn, struct context *context) {
    char *linebuf = malloc(LINE_MAX * sizeof(char));
    if (linebuf == NULL) {
        return false;
    }

    ssize_t len = read_line(conn, linebuf, LINE_MAX * sizeof(char));
    if (len == -1) {
        free(linebuf);
        return false;
    } else if (linebuf[0] != '2') {
        // ignore errors and redirects here to avoid complcating things.
        free(linebuf);
        return true;
    }

    bool matches = false;
    for (;;) {
        ssize_t len = read_line(conn, linebuf, LINE_MAX * sizeof(char));
        if (len == -1) {
            free(linebuf);
            return false;
        } else if (len == 0) {
            break;
        }

        if (has_prefix_case(linebuf, "user-agent:")) {
            char const *agent = &linebuf[sizeof("user-agent:")-1];
            while (agent[0] == ' ') agent++;

            for (size_t i = 0; i < sizeof(user_agents) / sizeof(user_agents[0]);
                    i++) {
                if (strcasecmp(agent, user_agents[i]) == 0) {
                    matches = true;
                    break;
                }
            }
        } else if (matches) {
            bool allow;
            char const *path;
            if (has_prefix_case(linebuf, "disallow:")) {
                allow = false;
                path = &linebuf[sizeof("disallow:")-1];
            } else if (has_prefix_case(linebuf, "allow:")) {
                allow = true;
                path = &linebuf[sizeof("allow:")-1];
            } else {
                continue;
            }

            while (path[0] == ' ') path++;
            char *s = malloc(sizeof(char) * (strlen(path) + 1));
            if (s == NULL) {
                log_line("not enough memory to allocate allow list");
                free(linebuf);
                return false;
            }
            strcpy(s, path);
            struct allow_path *allow_list = realloc(context->allow_list,
                sizeof(*context->allow_list) * ++context->allow_list_size);
            if (allow_list == NULL) {
                log_line("not enough memory to allocate allow list");
                free(s);
                free(linebuf);
                return false;
            }
            allow_list[context->allow_list_size-1].s = s;
            allow_list[context->allow_list_size-1].allow = allow;
            context->allow_list = allow_list;
        }
    }

    free(linebuf);
    return true;
}

static bool create_domain_directory(void *conn, char const *domain) {
    if (mkdir(config.archiver.archive_path, 0755) == -1 && errno != EEXIST) {
        log_format("failed to create archive directory: %s",
            errno_string(errno));
        write_header(conn, STATUS_TEMPORARY_FAILURE,
            "Something went wrong when indexing page");
        return false;
    }

    char const *dirs[] = {
        domain,
        "content",
    };
    for (size_t i = 0; i < sizeof(dirs) / sizeof(dirs[0]); i++) {
        char *path = join_path(config.archiver.archive_path, dirs[i], NULL);
        if (path == NULL) {
            log_line("not enough memory to allocate archive path");
            write_header(conn, STATUS_SERVER_UNAVAILABLE, out_of_memory_msg);
            return false;
        }
        if (mkdir(path, 0755) == -1 && errno != EEXIST) {
            log_format("failed to create index directory: %s",
                errno_string(errno));
            free(path);
            write_header(conn, STATUS_TEMPORARY_FAILURE,
                "Something went wrong when indexing page");
            return false;
        }
        free(path);
    }
    return true;
}

static bool is_recently_archived(void *conn, char const *domain) {
    char *path = join_path(config.archiver.archive_path, domain, "index", NULL);
    if (path == NULL) {
        log_line("not enough memory to allocate archive path");
        write_header(conn, STATUS_SERVER_UNAVAILABLE, out_of_memory_msg);
        return true;
    }
    struct stat st;
    if (stat(path, &st) == -1) {
        if (errno == ENOENT) {
            return false;
        }
        log_format("failed to create index directory: %s",
            errno_string(errno));
        free(path);
        write_header(conn, STATUS_TEMPORARY_FAILURE,
            "Something went wrong when indexing page");
        return true;
    }
    free(path);
    if (st.st_mtime + config.archiver.archive_interval >= time(NULL)) {
        write_header(conn, STATUS_TEMPORARY_FAILURE,
            "Domain was recently archived, please try again later");
        return true;
    }
    return false;
}

static char const lock_prefix[] = "/tmp/nostalgia/lock";
static char *lock_file = NULL;

static bool lock_domain(struct uri *uri) {
    if (mkdir("/tmp/nostalgia", 0700) == -1 && errno != EEXIST) {
        log_format("failed to create domain lock directory: %s",
            strerror(errno));
        return false;
    }
    if (mkdir("/tmp/nostalgia/lock", 0700) == -1 && errno != EEXIST) {
        log_format("failed to create domain lock directory: %s",
            strerror(errno));
        return false;
    }
    assert(lock_file == NULL);
    lock_file = join_path(lock_prefix, uri->domain, NULL);
    if (lock_file == NULL) {
        log_line("not enough memory to allocate lock file path");
        return false;
    }
    int fd = open(lock_file, O_RDWR | O_CREAT | O_EXCL, 0700);
    if (fd == -1) {
        if (errno != EEXIST) {
            log_format("failed to create lock file for %s: %s", uri->domain,
                strerror(errno));
        }
        free(lock_file);
        lock_file = NULL;
        return false;
    }

    // also write pid, so we can clean up if the process ever crashes.
    pid_t pid = getpid();
    if (dprintf(fd, "%l\n", (long)pid) == -1) {
        log_format("failed to write pid to domain lock: %s", strerror(errno));
        close(fd);
        unlink(lock_file);
        free(lock_file);
        lock_file = NULL;
        return false;
    }
    close(fd);
    return true;
}

static void unlock_domain(struct uri *uri) {
    assert(lock_file != NULL);
    if (unlink(lock_file) == -1) {
        log_format("failed to unlock domain %s: %s", uri->domain,
            strerror(errno));
        return;
    }
    free(lock_file);
    lock_file = NULL;
}

static bool is_domain_blacklisted(char const *domain) {
    size_t domain_len = strlen(domain);

    char *buf = malloc(sizeof(char) * 256);
    if (buf == NULL) {
        log_line("not enough memory to allocate domain buffer");
        return true;
    }
    int fd = open(config.archiver.blacklist_path, O_RDONLY);
    if (fd == -1) {
        free(buf);
        if (errno == ENOENT) {
            log_format("blacklist file %s doesn't exist, domain is accepted",
                config.archiver.blacklist_path);
            return false;
        } else {
            log_format("failed to open %s: %s", config.archiver.blacklist_path,
                errno_string(errno));
            return true;
        }
    }

    for (;;) {
        ssize_t len = read_file_line(fd, buf, sizeof(char) * 256);
        if (len == -1) {
            log_format("failed to read blacklist: %s", errno_string(errno));
            close(fd);
            free(buf);
            return true;
        } else if (len == 0) {
            break;
        } else if (len >= sizeof(char) * 256) {
            log_line("blacklist has errors: one or more lines are too long");
            continue;
        }

        if (buf[0] == '*') {
            size_t buf_len = strlen(buf);
            if (domain_len >= buf_len - 1) {
                if (strcmp(&buf[1], &domain[domain_len - buf_len + 1]) == 0) {
                    close(fd);
                    free(buf);
                    return true;
                }
            }
        } else {
            if (strcmp(buf, domain) == 0) {
                close(fd);
                free(buf);
                return true;
            }
        }
    }

    close(fd);
    free(buf);
    return false;
}

bool invoke_crawler(void *conn, struct uri *uri) {
    struct context context;

    context.uri = uri;
    context.redirect_count = 0;
    context.allow_list = NULL;
    context.allow_list_size = 0;
    context.last_request.tv_sec = 0;
    context.last_request.tv_nsec = 0;
    if (uri->port & ~0xffff) {
        write_header(conn, STATUS_PERMANENT_FAILURE, "Domain is invalid");
        return false;
    }
    size_t len = snprintf(context.port_str, sizeof(context.port_str), "%d",
        uri->port);
    assert(len < sizeof(context.port_str));

    if (!resolve_domain(uri->domain, context.port_str, &context.sa)) {
        write_header(conn, STATUS_PERMANENT_FAILURE, "Domain is invalid");
        return false;
    }

    if (is_domain_blacklisted(uri->domain)) {
        write_header(conn, STATUS_PERMANENT_FAILURE, "Domain is blacklisted");
        return false;
    }

    if (!lock_domain(uri)) {
        if (errno == EEXIST) {
            write_header(conn, STATUS_TEMPORARY_FAILURE,
                "Domain is currently being archived, please try again later");
        } else {
            write_header(conn, STATUS_TEMPORARY_FAILURE,
                "Something went wrong when indexing page");
        }
        return false;
    }

    if (is_recently_archived(conn, uri->domain)) {
        unlock_domain(uri);
        return false;
    }

    struct uri robots_uri;
    memcpy(&robots_uri, uri, sizeof(struct uri));
    robots_uri.path = "/robots.txt";
    robots_uri.query = NULL;
    context.uri = &robots_uri;
    if (!send_request(&context, parse_robots)) {
        while (context.allow_list_size-- > 0) {
            free(context.allow_list[context.allow_list_size].s);
        }
        free(context.allow_list);
        unlock_domain(uri);
        write_header(conn, STATUS_TEMPORARY_FAILURE,
            "Something went wrong when indexing page");
        return false;
    }

    if (!create_domain_directory(conn, uri->domain)) {
        while (context.allow_list_size-- > 0) {
            free(context.allow_list[context.allow_list_size].s);
        }
        free(context.allow_list);
        unlock_domain(uri);
        return false;
    }

    context.uri = uri;
    if (!send_request(&context, handle_response)) {
        while (context.allow_list_size-- > 0) {
            free(context.allow_list[context.allow_list_size].s);
        }
        free(context.allow_list);
        unlock_domain(uri);
        write_header(conn, STATUS_TEMPORARY_FAILURE,
            "Something went wrong when indexing page");
        return false;
    }
    while (context.allow_list_size-- > 0) {
        free(context.allow_list[context.allow_list_size].s);
    }
    free(context.allow_list);
    unlock_domain(uri);
    return true;
}
