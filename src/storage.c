/*
    Copyright (C) 2022  Gustaf "Hanicef" Alhäll

    This file is part of Nostalgia Project.

    Nostalgia Project is free software: you can redistribute it and/or modify it
    under the terms of the GNU Affero General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    Nostalgia Project is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Nostalgia Project. If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <assert.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "config.h"
#include "storage.h"
#include "utils.h"
#include "hash.h"

#define CTYPE_MAX 256

#define B_TREE_BLOCK_SIZE 4096

enum node_type {
    NODE_LEAF = 0,
    NODE_INTERNAL = 1,
};

struct pair {
    uint8_t key[HASH_WIDTH];
    uint64_t value;
};

union b_tree {
    struct {
        uint8_t type;
        uint16_t size;
        uint8_t padding[B_TREE_BLOCK_SIZE - sizeof(uint32_t)];
    };
    struct {
        uint8_t type;
        uint16_t size;
        struct pair values[B_TREE_BLOCK_SIZE /
            (sizeof(struct pair) + sizeof(uint64_t)) - 1];
        uint64_t nodes[B_TREE_BLOCK_SIZE /
            (sizeof(struct pair) + sizeof(uint64_t))];
    } internal;
    struct {
        uint8_t type;
        uint16_t size;
        struct pair values[B_TREE_BLOCK_SIZE / sizeof(struct pair)];
    } leaf;
};

struct entry {
    uint64_t next;
    uint64_t date;
    char content_hash[HASH_WIDTH];
    uint8_t ctype_len;
    // char ctype[];
};

static_assert(sizeof(union b_tree) == B_TREE_BLOCK_SIZE);
static_assert(SIZEOF_ARRAY(((union b_tree){}).internal.values) ==
    SIZEOF_ARRAY(((union b_tree){}).internal.nodes) - 1);
static_assert(SIZEOF_ARRAY(((union b_tree){}).internal.nodes) >= 2);

static bool create_tree(int fd, char const hash[static HASH_WIDTH]) {
    struct stat st;
    if (fstat(fd, &st) == -1) {
        return false;
    }
    if (st.st_size != 0) {
        errno = EFAULT;
        return false;
    }
    if (ftruncate(fd, B_TREE_BLOCK_SIZE) == -1) {
        return false;
    }
    char type = NODE_LEAF;
    if (write(fd, &type, 1) == -1) {
        return false;
    }
    if (lseek(fd, offsetof(union b_tree, size), SEEK_SET) == -1) {
        return false;
    }
    uint16_t size = 1;
    if (write(fd, &size, sizeof(size)) == -1) {
        return false;
    }
    if (lseek(fd, offsetof(union b_tree, internal.values), SEEK_SET) == -1) {
        return false;
    }
    if (write(fd, hash, HASH_WIDTH) == -1) {
        return false;
    }
    // no need to write offset, ftruncate should've already filled it with 0.
    return true;
}

static union b_tree *map_tree(int fd, size_t *size, bool write) {
    assert(size != NULL);
    struct stat st;
    if (fstat(fd, &st) == -1) {
        return NULL;
    }
    if (st.st_size % B_TREE_BLOCK_SIZE != 0) {
        errno = EFAULT;
        return NULL;
    }
    int flags = PROT_READ;
    if (write) {
        flags |= PROT_WRITE;
    }
    union b_tree *tree = mmap(NULL, st.st_size + B_TREE_BLOCK_SIZE * 20, flags,
        MAP_SHARED, fd, 0);
    if (tree == MAP_FAILED) {
        return NULL;
    }
    *size = st.st_size;
    return tree;
}

static union b_tree *extend_tree(union b_tree *tree, int fd, int blocks,
        size_t *size) {
    struct stat st;
    if (fstat(fd, &st) == -1) {
        return NULL;
    }
    if (st.st_size % B_TREE_BLOCK_SIZE != 0) {
        errno = EFAULT;
        return NULL;
    }
    size_t new_size = st.st_size + blocks * B_TREE_BLOCK_SIZE;
    // allocate the disk space before mapping to avoid fragmentation.
    if (posix_fallocate(fd, st.st_size, blocks * B_TREE_BLOCK_SIZE) == -1) {
        return NULL;
    }
    union b_tree *new_tree = mmap(NULL, new_size, PROT_READ | PROT_WRITE,
        MAP_SHARED, fd, 0);
    if (new_tree == MAP_FAILED) {
        ftruncate(fd, st.st_size);
        return NULL;
    }
    munmap(tree, *size);
    *size = new_size;
    return new_tree;
}

static enum search_status search(union b_tree *node, uint64_t *next,
        char key[static HASH_WIDTH], uint64_t *value) {
    if (node->type == NODE_LEAF) {
        for (size_t i = 0; i < node->size; i++) {
            if (memcmp(node->leaf.values[i].key, key, HASH_WIDTH) == 0) {
                if (value != NULL) {
                    *value = node->leaf.values[i].value;
                }
                return SEARCH_MATCH;
            }
        }
        return SEARCH_NO_MATCH;
    } else {
        for (size_t i = 0; i < node->size; i++) {
            if (memcmp(node->internal.values[i].key, key, HASH_WIDTH) == 0) {
                if (value != NULL) {
                    *value = node->internal.values[i].value;
                }
                return SEARCH_MATCH;
            }
            if (memcmp(key, node->internal.values[i].key, HASH_WIDTH) < 0 &&
                (i == 0 || memcmp(key, node->internal.values[i-1].key,
                    HASH_WIDTH) > 0)) {
                *next = node->internal.nodes[i];
                return SEARCH_CONTINUE;
            }
        }
        *next = node->internal.nodes[node->size];
        return SEARCH_CONTINUE;
    }
}

static enum search_status find_value(union b_tree *tree,
        char key[static HASH_WIDTH], uint64_t *value) {
    uint64_t next = 0;
    for (;;) {
        enum search_status status = search(&tree[next], &next, key, value);
        if (status != SEARCH_CONTINUE) {
            return status;
        }
    }
}

static void insert_leaf(union b_tree *node, struct pair *pair) {
    assert(node->type == NODE_LEAF);
    size_t i;
    for (i = 0; i < node->size; i++) {
        assert(memcmp(node->leaf.values[i].key, pair->key, HASH_WIDTH) != 0);
        if (memcmp(pair->key, node->leaf.values[i].key, HASH_WIDTH) < 0) {
            for (size_t j = node->size; j > i; j--) {
                node->leaf.values[j] = node->leaf.values[j-1];
            }
            break;
        }
    }
    memcpy(node->leaf.values[i].key, pair->key, HASH_WIDTH);
    node->leaf.values[i].value = pair->value;
    node->size++;
}

static union b_tree *insert_internal(union b_tree *tree, int fd, uint64_t index,
        struct pair *pair, size_t *size) {
    union b_tree *node = &tree[index];
    assert(node->type == NODE_INTERNAL);

    uint64_t value, next = 0;
    enum search_status result = search(node, &next, pair->key, NULL);
    assert(result != SEARCH_NO_MATCH);
    if (result == SEARCH_ERROR) {
        return NULL;
    } else if (result == SEARCH_MATCH) {
        // value already exists, which is fine.
        return tree;
    }

    union b_tree *child = &tree[next];
    if (child->type == NODE_LEAF) {
        insert_leaf(child, pair);
        if (child->size >= SIZEOF_ARRAY(child->leaf.values)) {
            tree = extend_tree(tree, fd, 1, size);
            if (tree == NULL) {
                return NULL;
            }

            // addresses might've been moved when we extended the tree
            child = &tree[next];
            node = &tree[index];
            union b_tree *upper = &tree[*size / B_TREE_BLOCK_SIZE - 1];
            upper->type = NODE_LEAF;
            upper->size = child->size / 2;
            memcpy(upper->leaf.values,
                &child->leaf.values[child->size - upper->size],
                upper->size * sizeof(struct pair));
#ifndef NDEBUG
            memset(&child->leaf.values[child->size - upper->size], -1,
                upper->size * sizeof(struct pair));
#endif
            child->size = child->size - upper->size - 1;

            size_t i;
            for (i = 0; i < node->size; i++) {
                if (memcmp(child->leaf.values[child->size].key,
                        node->internal.values[i].key, HASH_WIDTH) < 0) {
                    for (size_t j = node->size; j > i; j--) {
                        node->internal.values[j] = node->internal.values[j-1];
                        node->internal.nodes[j+1] = node->internal.nodes[j];
                    }
                    break;
                }
            }
            node->internal.values[i] = child->leaf.values[child->size];
            node->internal.nodes[i+1] = upper - tree;
            node->size++;
        }
    } else {
        tree = insert_internal(tree, fd, next, pair, size);
        if (tree == NULL) {
            return NULL;
        }

        // earlier calls might've expanded the tree, which invalidates addresses
        child = &tree[next];
        if (child->size >= SIZEOF_ARRAY(child->internal.values)) {
            tree = extend_tree(tree, fd, 1, size);
            if (tree == NULL) {
                return NULL;
            }

            // addresses might've been moved when we extended the tree
            child = &tree[next];
            node = &tree[index];
            union b_tree *upper = &tree[*size / B_TREE_BLOCK_SIZE - 1];
            upper->type = NODE_INTERNAL;
            upper->size = child->size / 2;
            memcpy(upper->internal.values,
                &child->internal.values[child->size - upper->size],
                upper->size * sizeof(struct pair));
            memcpy(upper->internal.nodes,
                &child->internal.nodes[child->size - upper->size],
                (upper->size + 1) * sizeof(uint64_t));
#ifndef NDEBUG
            memset(&child->internal.values[child->size - upper->size], -1,
                upper->size * sizeof(struct pair));
            memset(&child->internal.nodes[child->size - upper->size], -1,
                (upper->size + 1) * sizeof(uint64_t));
#endif
            child->size = child->size - upper->size - 1;

            size_t i;
            for (i = 0; i < node->size; i++) {
                if (memcmp(child->internal.values[child->size].key,
                        node->internal.values[i].key, HASH_WIDTH) < 0) {
                    for (size_t j = node->size; j > i; j--) {
                        node->internal.values[j] = node->internal.values[j-1];
                        node->internal.nodes[j+1] = node->internal.nodes[j];
                    }
                    break;
                }
            }
            node->internal.values[i] = child->internal.values[child->size];
            node->internal.nodes[i+1] = upper - tree;
            node->size++;
        }
    }
    return tree;
}

static union b_tree *insert(union b_tree *tree, int fd, struct pair *pair,
        size_t *size) {
    if (tree->type == NODE_LEAF) {
        insert_leaf(tree, pair);
        if (tree->size >= SIZEOF_ARRAY(tree->leaf.values)) {
            tree = extend_tree(tree, fd, 2, size);
            if (tree == NULL) {
                return NULL;
            }

            union b_tree *lower = &tree[*size / B_TREE_BLOCK_SIZE - 2];
            union b_tree *upper = &tree[*size / B_TREE_BLOCK_SIZE - 1];
            upper->type = NODE_LEAF;
            upper->size = tree->size / 2;
            lower->type = NODE_LEAF;
            lower->size = tree->size - upper->size - 1;
            memcpy(lower->leaf.values, tree->leaf.values,
                lower->size * sizeof(struct pair));
            memcpy(upper->leaf.values, &tree->leaf.values[lower->size + 1],
                upper->size * sizeof(struct pair));

            tree->type = NODE_INTERNAL;
            tree->size = 1;
            tree->internal.values[0] = tree->leaf.values[lower->size];
            tree->internal.nodes[0] = lower - tree;
            tree->internal.nodes[1] = upper - tree;
#ifndef NDEBUG
            memset(&tree->internal.values[1], -1,
                sizeof(tree->internal.values) - sizeof(struct pair));
            memset(&tree->internal.nodes[2], -1,
                sizeof(tree->internal.nodes) - sizeof(uint64_t) * 2);
#endif
        }
    } else {
        tree = insert_internal(tree, fd, 0, pair, size);
        if (tree == NULL) {
            return NULL;
        }

        if (tree->size >= SIZEOF_ARRAY(tree->internal.values)) {
            tree = extend_tree(tree, fd, 2, size);
            if (tree == NULL) {
                return NULL;
            }

            union b_tree *lower = &tree[*size / B_TREE_BLOCK_SIZE - 2];
            union b_tree *upper = &tree[*size / B_TREE_BLOCK_SIZE - 1];
            upper->type = NODE_INTERNAL;
            upper->size = tree->size / 2;
            lower->type = NODE_INTERNAL;
            lower->size = tree->size - upper->size - 1;
            memcpy(lower->internal.values, tree->internal.values,
                lower->size * sizeof(struct pair));
            memcpy(lower->internal.nodes, tree->internal.nodes,
                (lower->size + 1) * sizeof(uint64_t));
            memcpy(upper->internal.values,
                &tree->internal.values[lower->size + 1],
                upper->size * sizeof(struct pair));
            memcpy(upper->internal.nodes,
                &tree->internal.nodes[lower->size + 1],
                (upper->size + 1) * sizeof(uint64_t));

            tree->type = NODE_INTERNAL;
            tree->size = 1;
            tree->internal.values[0] = tree->internal.values[lower->size];
            tree->internal.nodes[0] = lower - tree;
            tree->internal.nodes[1] = upper - tree;
#ifndef NDEBUG
            memset(&tree->internal.values[1], -1,
                sizeof(tree->internal.values) - sizeof(struct pair));
            memset(&tree->internal.nodes[2], -1,
                sizeof(tree->internal.nodes) - sizeof(uint64_t) * 2);
#endif
        }
    }
    return tree;
}

static ssize_t write_16(int fd, uint16_t v) {
    char s[2] = { v & 0xff, v >> 8 };
    return write(fd, s, 2);
}

static ssize_t read_16(int fd, uint16_t *v) {
    char s[2];
    ssize_t n = read(fd, s, sizeof(s));
    if (n != sizeof(s)) {
        return n;
    }

    *v = (uint16_t)s[0] | ((uint16_t)s[1] << 8);
    return n;
}

static bool write_entry(int fd, uint64_t *offset, time_t date,
        char const *ctype, char const content_hash[static HASH_WIDTH]) {
    off_t end = lseek(fd, 0, SEEK_END);
    if (end == -1) {
        return false;
    }

    size_t ctype_len = strlen(ctype);
    struct entry entry;
    entry.next = 0;
    entry.date = date;
    memcpy(entry.content_hash, content_hash, HASH_WIDTH);
    entry.ctype_len = ctype_len;

    if (write(fd, &entry, sizeof(struct entry)) == -1) {
        return false;
    }
    if (write(fd, ctype, ctype_len) == -1) {
        return false;
    }
    if (offset != NULL && *offset != -1) {
        while (*offset != 0) {
            if (lseek(fd, *offset, SEEK_SET) == -1) {
                ftruncate(fd, end);
                return false;
            }
            if (read(fd, offset, sizeof(*offset)) == -1) {
                ftruncate(fd, end);
                return false;
            }
        }

        if (lseek(fd, -8, SEEK_CUR) == -1) {
            ftruncate(fd, end);
            return false;
        }
        uint64_t v = end;
        if (write(fd, &v, sizeof(v)) == -1) {
            ftruncate(fd, end);
            return false;
        }
    }
    if (offset != NULL) {
        *offset = end;
    }
    return true;
}

static bool read_entry(int fd, uint64_t *offset, uint64_t *date,
        char *ctype_buf, char content_hash[static HASH_WIDTH]) {
    struct entry entry;
    if (read(fd, &entry, sizeof(entry)) == -1) {
        return false;
    }
    *offset = entry.next;
    *date = entry.date;
    memcpy(content_hash, entry.content_hash, HASH_WIDTH);
    if (read(fd, ctype_buf, entry.ctype_len) == -1) {
        return false;
    }
    ctype_buf[entry.ctype_len] = '\0';
    return true;
}

bool add_index(char const *domain, char const *path, time_t date,
        char const *ctype, char const content_hash[static HASH_WIDTH]) {
    char *entry_path = join_path(config.archiver.archive_path, domain,
        "entries", NULL);
    if (entry_path == NULL) {
        return false;
    }
    int fd = open(entry_path, O_RDWR | O_CREAT, 0644);
    if (fd == -1) {
        free(entry_path);
        return false;
    }
    free(entry_path);

    struct hash *hash = hash_init();
    if (hash == NULL) {
        close(fd);
        return false;
    }
    size_t path_len = strlen(path);
    hash_insert(hash, path, path_len);
    char index_hash[HASH_WIDTH];
    hash_digest(hash, index_hash);
    free(hash);

    char *index_path = join_path(config.archiver.archive_path, domain, "index",
        NULL);
    if (index_path == NULL) {
        close(fd);
        return false;
    }

    uint64_t offset;
    int index_fd = open(index_path, O_RDWR, 0644);
    if (index_fd == -1) {
        if (errno != ENOENT) {
            free(index_path);
            close(fd);
            return false;
        }

        index_fd = open(index_path, O_WRONLY | O_CREAT | O_EXCL, 0644);
        if (index_fd == -1) {
            free(index_path);
            close(fd);
            return false;
        }
        free(index_path);
        if (!create_tree(index_fd, index_hash)) {
            close(index_fd);
            close(fd);
            return false;
        }

        if (!write_entry(fd, NULL, date, ctype, content_hash)) {
            close(index_fd);
            close(fd);
            return false;
        }
    } else {
        free(index_path);
        size_t tree_size;
        union b_tree *tree = map_tree(index_fd, &tree_size, true);
        if (tree == NULL) {
            close(index_fd);
            close(fd);
            return false;
        }

        if (find_value(tree, index_hash, &offset) == SEARCH_NO_MATCH) {
            offset = -1;
        }

        if (!write_entry(fd, &offset, date, ctype, content_hash)) {
            munmap(tree, tree_size);
            close(index_fd);
            close(fd);
            return false;
        }

        struct pair pair;
        memcpy(&pair.key, index_hash, HASH_WIDTH);
        pair.value = offset;
        union b_tree *new_tree = insert(tree, index_fd, &pair, &tree_size);
        if (new_tree == NULL) {
            munmap(tree, tree_size);
            close(index_fd);
            close(fd);
            return false;
        }
        munmap(tree, tree_size);
    }

    close(index_fd);
    close(fd);
    return true;
}

enum search_status search_index(char const *domain, char const *path,
        search_fn callback, void *data) {
    char *index_path = join_path(config.archiver.archive_path, domain, "index",
        NULL);
    if (index_path == NULL) {
        return SEARCH_ERROR;
    }
    int index_fd = open(index_path, O_RDONLY, 0644);
    if (index_fd == -1) {
        free(index_path);
        if (errno == ENOENT) {
            return SEARCH_NO_MATCH;
        }
        return SEARCH_ERROR;
    }
    free(index_path);

    size_t tree_size;
    union b_tree *tree = map_tree(index_fd, &tree_size, false);
    if (tree == NULL) {
        close(index_fd);
        return SEARCH_ERROR;
    }
    struct hash *hash = hash_init();
    if (hash == NULL) {
        munmap(tree, tree_size);
        close(index_fd);
        return SEARCH_ERROR;
    }
    size_t path_len = strlen(path);
    hash_insert(hash, path, path_len);
    char index_hash[HASH_WIDTH];
    hash_digest(hash, index_hash);
    free(hash);

    uint64_t offset;
    enum search_status status = find_value(tree, index_hash, &offset);
    if (status == SEARCH_ERROR || status == SEARCH_NO_MATCH) {
        munmap(tree, tree_size);
        close(index_fd);
        return status;
    }
    munmap(tree, tree_size);
    close(index_fd);

    char *entry_path = join_path(config.archiver.archive_path, domain,
        "entries", NULL);
    if (entry_path == NULL) {
        return SEARCH_ERROR;
    }
    int fd = open(entry_path, O_RDONLY);
    if (fd == -1) {
        free(entry_path);
        return SEARCH_ERROR;
    }
    free(entry_path);

    char *ctype_buf = malloc(CTYPE_MAX + 1);
    if (ctype_buf == NULL) {
        close(fd);
        return SEARCH_ERROR;
    }

    if (lseek(fd, offset, SEEK_SET) == -1) {
        close(fd);
        free(ctype_buf);
        return SEARCH_ERROR;
    }
    do {
        uint64_t date;
        char content_hash[HASH_WIDTH];
        if (!read_entry(fd, &offset, &date, ctype_buf, content_hash)) {
            close(fd);
            free(ctype_buf);
            return SEARCH_ERROR;
        }
        if (callback == NULL) {
            close(fd);
            free(ctype_buf);
            return SEARCH_MATCH;
        }
        enum search_status status = callback(date, content_hash, ctype_buf,
            data);
        if (status != SEARCH_NO_MATCH) {
            close(fd);
            free(ctype_buf);
            return status;
        }
    } while (offset != 0);
    close(fd);
    return SEARCH_NO_MATCH;
}

bool add_redirect(int fd, char const *from, char const *to) {
    off_t offset = lseek(fd, 0, SEEK_CUR);
    if (offset == -1) {
        return false;
    }

    size_t from_len = strlen(from);
    size_t to_len = strlen(to);

    if (write_16(fd, from_len) == -1) {
        ftruncate(fd, offset);
        return false;
    }
    if (write(fd, from, from_len) == -1) {
        ftruncate(fd, offset);
        return false;
    }

    if (write_16(fd, to_len) == -1) {
        ftruncate(fd, offset);
        return false;
    }
    if (write(fd, to, to_len) == -1) {
        ftruncate(fd, offset);
        return false;
    }
    return true;
}

bool has_redirect(int fd, char const *from, char const *to) {
    unsigned char s[2];
    size_t from_len = strlen(from);
    size_t to_len = strlen(to);

    char *buf = malloc(sizeof(char) * from_len);
    if (buf == false) {
        return false;
    }

    for (;;) {
        uint16_t len;
        ssize_t n = read_16(fd, &len);
        if (n == -1) {
            free(buf);
            return false;
        } else if (n == 0) {
            free(buf);
            return false;
        } else if (n != 2) {
            free(buf);
            errno = ENXIO;
            return false;
        }

        if (len != from_len) {
            if (lseek(fd, len, SEEK_CUR) == -1) {
                free(buf);
                return false;
            }
            n = read_16(fd, &len);
            if (n != 2) {
                if (n != -1) {
                    errno = ENXIO;
                }
                free(buf);
                return false;
            }

            if (lseek(fd, len, SEEK_CUR) == -1) {
                free(buf);
                return false;
            }
            continue;
        }

        n = read(fd, buf, len);
        if (n != len) {
            if (n != -1) {
                errno = ENXIO;
            }
            free(buf);
            return false;
        }

        n = read_16(fd, &len);
        if (n != 2) {
            if (n != -1) {
                errno = ENXIO;
            }
            free(buf);
            return false;
        }

        if (len != to_len || memcmp(from, buf, from_len) != 0) {
            if (lseek(fd, to_len, SEEK_CUR) == -1) {
                free(buf);
                return false;
            }
        } else {
            n = read(fd, buf, len);
            if (n != len) {
                if (n != -1) {
                    errno = ENXIO;
                }
                free(buf);
                return false;
            }
            if (memcmp(to, buf, to_len) == 0) {
                return true;
            }
        }
    }
}

char **get_redirects(int fd, char const *from, size_t *count) {
    unsigned char s[2];
    char **to_list = NULL;
    *count = 0;

    size_t from_len = strlen(from);
    char *buf = malloc(sizeof(char) * from_len);
    if (buf == NULL) {
        return NULL;
    }

    for (;;) {
        uint16_t len;
        ssize_t n = read_16(fd, &len);
        if (n == -1) {
            while ((ssize_t)--*count >= 0) {
                free(to_list[*count]);
            }
            free(to_list);
            free(buf);
            return NULL;
        } else if (n == 0) {
            break;
        } else if (n != 2) {
            while ((ssize_t)--*count >= 0) {
                free(to_list[*count]);
            }
            free(to_list);
            free(buf);
            errno = ENXIO;
            return NULL;
        }

        if (len != from_len) {
            if (lseek(fd, len, SEEK_CUR) == -1) {
                while ((ssize_t)--*count >= 0) {
                    free(to_list[*count]);
                }
                free(to_list);
                free(buf);
                return NULL;
            }
            n = read_16(fd, &len);
            if (n != 2) {
                if (n != -1) {
                    errno = ENXIO;
                }
                while ((ssize_t)--*count >= 0) {
                    free(to_list[*count]);
                }
                free(to_list);
                free(buf);
                return NULL;
            }

            if (lseek(fd, len, SEEK_CUR) == -1) {
                while ((ssize_t)--*count >= 0) {
                    free(to_list[*count]);
                }
                free(to_list);
                free(buf);
                return NULL;
            }
            continue;
        }

        n = read(fd, buf, len);
        if (n != len) {
            if (n != -1) {
                errno = ENXIO;
            }
            while ((ssize_t)--*count >= 0) {
                free(to_list[*count]);
            }
            free(to_list);
            free(buf);
            return NULL;
        }

        uint16_t to_len;
        n = read_16(fd, &to_len);
        if (n != 2) {
            if (n != -1) {
                errno = ENXIO;
            }
            while ((ssize_t)--*count >= 0) {
                free(to_list[*count]);
            }
            free(to_list);
            free(buf);
            return NULL;
        }

        if (memcmp(from, buf, len) != 0) {
            if (lseek(fd, to_len, SEEK_CUR) == -1) {
                while ((ssize_t)--*count >= 0) {
                    free(to_list[*count]);
                }
                free(to_list);
                free(buf);
                return NULL;
            }
            continue;
        }

        char *to = malloc(sizeof(char) * (to_len + 1));
        if (to == NULL) {
            while ((ssize_t)--*count >= 0) {
                free(to_list[*count]);
            }
            free(to_list);
            free(buf);
            return NULL;
        }

        n = read(fd, to, to_len);
        if (n != to_len) {
            if (n == -1) {
                errno = ENXIO;
            }
            while ((ssize_t)--*count >= 0) {
                free(to_list[*count]);
            }
            free(to_list);
            free(to);
            free(buf);
            return NULL;
        }
        to[to_len] = '\0';

        char **tmp = realloc(to_list, sizeof(char *) + ++*count);
        if (tmp == NULL) {
            if (n == -1) {
                errno = ENXIO;
            }
            --*count;
            while ((ssize_t)--*count >= 0) {
                free(to_list[*count]);
            }
            free(to_list);
            free(to);
            free(buf);
            return NULL;
        }
        to_list = tmp;
        to_list[*count-1] = to;
    }
    free(buf);
    return to_list;
}
