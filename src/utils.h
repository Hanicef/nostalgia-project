/*
    Copyright (C) 2022  Gustaf "Hanicef" Alhäll

    This file is part of Nostalgia Project.

    Nostalgia Project is free software: you can redistribute it and/or modify it
    under the terms of the GNU Affero General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    Nostalgia Project is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Nostalgia Project. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UTILS_H
#define UTILS_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define SIZEOF_ARRAY(x) (sizeof(x) / sizeof((x)[0]))

struct string_buffer {
    char *s;
    size_t cap;
    size_t size;
};

bool init_string(struct string_buffer *buf, size_t cap);
void append_string(struct string_buffer *buf, char const *s);
char *close_string(struct string_buffer *buf);

#ifndef __attribute__
# define __attribute__(x)
#endif

extern char const *log_file;

void log_line(char const *s);
__attribute__((format(printf, 1, 2)))
void log_format(char const *format, ...);

bool has_prefix(char const *s, char const *prefix);
bool has_prefix_case(char const *s, char const *prefix);

char *join_path(char const *s, ...);
char **split_path(char const *s, size_t *count, size_t max);

char *format_hex_string(char s[static 17], uint64_t v);
size_t format_base64(char *s, size_t len, char const *data, size_t dlen);

char const *errno_string(int err);

#endif // UTILS_H
